
#+STARTUP: showall

#+TITLE: Eclats de vers : Litéra 05 : Babel
#+AUTHOR: chimay
#+EMAIL: or du val chez gé courriel commercial
#+LANGUAGE: fr
#+LINK_HOME: file:../index.html
#+LINK_UP: file:index.html
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../style/defaut.css" />

#+OPTIONS: H:6
#+OPTIONS: toc:nil

#+TAGS: noexport(n)

[[file:index.org][Index des Grimoires]]

#+INCLUDE: "../include/navigan-1.org"

#+TOC: headlines 1

* Grec

| Racine          | Signification              | Exemples                        |
|-----------------+----------------------------+---------------------------------|
| A, An           | Absence, Privation         | Aphone, Anonyme                 |
| All, Allo       | Autre                      | Allergie, Allogamie, Allégorie  |
| Amphi           | Des deux côtés, Autour     | Amphithéâtre                    |
|                 | Retour, Reprise,           |                                 |
| Ana             | Arrière, Sens inverse,     | Anadiplose, Anagramme, Analogie |
|                 | En tête, En remontant      |                                 |
| Anémo           | Vent                       | Anémomètre, Anémone             |
| Anta            | En face de, devant         | (identique à anti ?)            |
| Antho           | Fleur                      |                                 |
| Anti            | Contre, opposé             |                                 |
| Apo             | Loin de                    |                                 |
| Astro           | Étoile                     |                                 |
| Bar, Baro       | Pesanteur                  |                                 |
| Bathy, Batho    | Profond, Creux             |                                 |
| Bole            | Jeter                      |                                 |
| Brachy          | Court                      |                                 |
| Caco            | Mauvais                    |                                 |
| Calli           | Beau                       |                                 |
| Cata            | En dessous, en bas         |                                 |
| Chron, Chrono   | Temps                      |                                 |
| Coque           | Graine                     |                                 |
| Crypt, Crypto   | Caché                      |                                 |
| Dactyl          | Doigt                      |                                 |
| Dia             | À travers                  |                                 |
| Dipl, Diplo     | Double                     |                                 |
| Endo            | Dedans                     |                                 |
| Gyr, Gyro       | Cercle                     |                                 |
| Épi             | Au-dessus, À la surface de |                                 |
| Eu              | Bien, Beau                 |                                 |
| Homo, Homéo     | Même, Semblable            |                                 |
| Hyper           | Au-dessus                  |                                 |
| Hypo            | En-dessous                 |                                 |
| Hypso           | Hauteur                    |                                 |
| Hystér, Hystéro | Matrice, Derrière          |                                 |
| Lipo            | Enlever, Laisser           |                                 |
| Machie          | Combat                     |                                 |
| Méta            | Au-delà                    |                                 |
| Néo             | Nouveau                    |                                 |
| Onomat, Onomato | Nom                        |                                 |
| Oxy             | Aigu, Fin                  |                                 |
| Pan             | Tout                       |                                 |
| Para            | Auprès de, À côté de       |                                 |
| Péri            | Autour de                  |                                 |
| Plasm           | Ouvrage                    |                                 |
| Poly            | Plusieurs                  |                                 |
| Prot, Proto     | Premier                    |                                 |
| Stat, Stas      | Base, Arrêt                |                                 |
| Syn             | Avec                       |                                 |


* Latin


** Racines

| Racine                | Signification           | Exemples  |
|-----------------------+-------------------------+-----------|
| Ab                    | Séparation, Éloignement |           |
| Ad                    | Vers, En direction de   |           |
| Ambi                  | Des deux côtés, Double  |           |
| Ante                  | Avant                   |           |
| Circon, Circum, Circa | Autour de, En rond      |           |
| Dis                   | Division                | Disloquer |
| Ex                    | En dehors de            |           |


** Expressions

| Expressions    | Signification |
|----------------+---------------|
| Exempli gratia | Par exemple   |
| Id est         | C’est-à-dire  |


* Italien


** Grammaire


*** Comparaison


**** Superlatif

-issim{o,a,i,e}

arci, super, iper, sovra, stra, extra, ultra + adj


***** Irréguliers

| Mot      | Signification | Mot ressemblant |
|----------+---------------+-----------------|
| buon     | bon           |                 |
| migliore | meilleur      |                 |
| ottimo   | très meilleur | optimal         |
|----------+---------------+-----------------|
| cattivo  | mauvais       |                 |
| peggiore | plus mauvais  |                 |
| pessimo  | très mauvais  | pessimiste      |
|----------+---------------+-----------------|
| grande   | grand         |                 |
| maggiore | plus grand    | majeur          |
| massimo  | très grand    | maximum         |
|----------+---------------+-----------------|
| piccolo  | petit         |                 |
| minore   | plus petit    | mineur          |
| minimo   | très petit    | minimum         |


** Mots

| Mot   | Signification | Mot ressemblant |
|-------+---------------+-----------------|
| molto | beaucoup      | moulte          |
| fiume | fleuve        |                 |
|       |               |                 |


** Verbes

| Verbe | Signification | Mot ressemblant |
|-------+---------------+-----------------|
|       |               |                 |



** Expressions

| Expression                   | Signification                               |
|------------------------------+---------------------------------------------|


* Anglais

Remarque : la traduction s’effectue au niveau des expressions, non au
niveau des mots. La table ci-dessous n’est pas destinée à remplacer un
bon vieux dictionnaire.

| Expression                   | Signification                               |
|------------------------------+---------------------------------------------|
| Allow me                     | Permettez                                   |
| As ... as can be             | Aussi ... que possible                      |
| As far as I am concerned ... | En ce qui me concerne ...                   |
| As of ...                    | À partir de ...                             |
| As opposed to                | Contrairement à                             |
| As well                      | Aussi bien                                  |
| As well as anybody           | Aussi bien que quiconque                    |
| At last !                    | Enfin !                                     |
| Besides, ...                 | En plus, ...                                |
| Be my guest                  | Je vous en prie                             |
|                              | (si vous voulez)                            |
| By all means                 | Je vous en prie                             |
| By any chance                | Par hasard                                  |
| By the way, ...              | Tant qu’on y est, ...                       |
| Chances are ...              | Il y a des chances que ...                  |
| Did he buy it ?              | Y a-t-il cru ?                              |
| Don’t go over my head        | Ne me court-circuite pas                    |
|                              | (au niveau hiérarchique)                    |
| Feel free to ...             | N’hésitez pas à ...                         |
| Figure of speech             | Figure de style                             |
| First and foremost           | D’abord et avant tout                       |
| For god sake !               | Pour l’amour de dieu !                      |
| Give it some thought !       | Pensez-y !                                  |
| Go figure !                  | Allez comprendre !                          |
| Good call                    | Bien vu !                                   |
| He is nowhere to be seen     | Personne ne l’a vu                          |
| He speaks through me         | Je parle en son nom                         |
| Help yourself                | Servez-vous                                 |
| He tails him                 | Il le suit                                  |
| How come (that) ...          | Comment se fait-il que ...                  |
| I got the picture            | Je vois l’idée                              |
| I’ll take care of those      | Je vais m’en occuper                        |
| I’m beside of myself         | Je suis à coté de mes pompes                |
| I’m not that ...             | Je ne suis pas si ...                       |
| I dare say                   | J’ose dire                                  |
|                              | Je dois dire                                |
| I don’t buy it               | Je n’y crois pas                            |
| I feel like dancing          | Je danserais bien                           |
| I myself                     | Moi-même                                    |
| I’m not that fool            | Je ne suis pas si stupide                   |
| I’m done                     | J’ai fini                                   |
| I say !                      | Çà alors !                                  |
| In tune with                 | En harmonie avec                            |
| In short                     | En un mot                                   |
|                              | En résumé                                   |
| It drives me crazy           | Ça me rend fou                              |
| It leaves much to be desired | Çà laisse à désirer                         |
| It’s far too ...             | C’est trop ..., et de loin !                |
| It’s way too ...             |                                             |
| It’s none of your business   | Ça ne te regarde pas                        |
| It’s not that bad            | Ce n’est pas si mal                         |
| Kind of ...                  | Assez, Plutôt ...                           |
| Leave it to me !             | Laisse, je m’en occupe !                    |
| Lucky me !                   | J’en ai de la chance ! (ironique)           |
| Meanwhile                    | Entre-temps                                 |
| Most kind !                  | Très gentil !                               |
| My mistake !                 | Au temps pour moi !                         |
| No kidding                   | Sans blague                                 |
| Oh wait ...                  | Oui mais attends ...                        |
| On the right track           | Sur la bonne voie                           |
| Pray                         | Je vous en prie (= allez-y, continuez)      |
| Shall we ?                   | On y va ?                                   |
| So far so good               | Jusque là, ça va                            |
| So much for ...              | Autant pour la réputation légendaire de ... |
| So to speak                  | pour ainsi dire                             |
| So true                      | Tellement vrai                              |
| Stands for                   | Veut dire                                   |
| Sure enough                  | Évidemment                                  |
| That’s all about ...         | Ce qui importe vraiment, c’est ...          |
| That’s the other way around  | C’est l’inverse                             |
| These days                   | De nos jours                                |
| Time to turn the tables      | Il est temps d’inverser les rôles           |
| To come and see you          | Passer vous voir                            |
| Twice in a row               | deux fois de suite                          |
| We shall call it ...         | On va dire que c’est ...                    |
| What is it you want ?        | Qu’est ce que tu veux ?                     |
| Would you care to ... ?      | Cela vous dirait-il de ... ?                |
| You are welcome              | Je vous en prie (= de rien)                 |
| What are you thinking ?      | Qu’est-ce que tu t’imagines ?               |
| Where it’s at                | Où ça se passe                              |
| You know where it’s at       | Tu sais où/comment ça se passe              |
| You know what they say       | Tu sais ce qu’on dit                        |
| Your every move              | Chacun de tes gestes                        |
