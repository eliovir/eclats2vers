
#+STARTUP: showall

#+TITLE: Eclats de vers : Noeuds 10 : Noeuds de maintien
#+AUTHOR: chimay
#+EMAIL: or du val chez gé courriel commercial
#+LANGUAGE: fr
#+LINK_HOME: file:../index.html
#+LINK_UP: file:index.html
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../style/defaut.css" />

#+OPTIONS: H:6
#+OPTIONS: toc:nil

#+TAGS: noexport(n)

[[file:index.org][Index des Grimoires]]

#+INCLUDE: "../include/navigan-1.org"

#+TOC: headlines 1

#+TAGS: noexport(n)
#+TAGS: derived(d)
#+TAGS: middle_rope(m)
#+TAGS: sliding(s) adjustable_grip(g)
#+TAGS: double_loop(2) triple_loop(3)
#+TAGS: quick_release(q)
#+TAGS: vibration_proof(v)

* Introduction

Les noeuds de maintien sont utilisés pour faire tenir des éléments
ensemble, ou serrer un colis.


* Packer’s knot

  1. faire une boucle
  2. faire un figure 8 autour de la partie statique
  3. serrer le figure 8
  4. serrer la boucle autour du colis
  5. terminer par un overhand


* Parcel knot

  1. faire une boucle autour du colis avec le premier terminus, partie
     de travail au-dessus
  2. rentrer le second terminus par en-dessous
  3. serrer la boucle
  4. pincer
  5. tendre
  6. terminer par un overhand


* Shopkeeper knot

  1. faire plusieurs tours bien répartis dans le sens de la longueur
     du colis
  2. faire un tour supplémentaire le long du précédant
  3. arrivé au point voulu, croiser la partie de corde longée et
     partir dans le sens de la largeur
  4. à chaque partie de corde croisée, faire une boucle autour
  5. réaliser ainsi un ou plusieurs tours dans le sens de la largeur
  6. revenir au milieu du colis
  7. terminer avec le noeud de votre choix
