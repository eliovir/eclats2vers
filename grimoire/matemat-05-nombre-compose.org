
#+STARTUP: showall

#+TITLE: Eclats de vers : Matemat 05 : Nombres composés
#+AUTHOR: chimay
#+EMAIL: or du val chez gé courriel commercial
#+LANGUAGE: fr
#+LINK_HOME: file:../index.html
#+LINK_UP: file:index.html
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../style/defaut.css" />

#+OPTIONS: H:6
#+OPTIONS: toc:nil

#+TAGS: noexport(n)

[[file:index.org][Index des Grimoires]]

#+INCLUDE: "../include/navigan-1.org"

#+TOC: headlines 1

#+INCLUDE: "../include/commandes-tex.org"

* Complexes

#+TOC: headlines 1 local

\label{chap:complexes}


** Dépendances

  - Chapitre \ref{chap:naturels} : Les nombres naturels
  - Chapitre \ref{chap:entiers} : Les nombres entiers
  - Chapitre \ref{chap:rationnels} : Les nombres rationnels
  - Chapitre \ref{chap:reels} : Les nombres réels


** Nombre imaginaire

Nous avons vu que le carré d'un nombre positif est forcément positif. Par conséquent, on ne peut pas trouver de $x \in \setR$ tel que :

$$x^2 = -1$$

Il nous faut donc inventer un nouvel objet, que l'on nomme nombre imaginaire, et que l'on note $\img$, tel que :

$$\img^2 = -1$$

On peut dès lors étendre la définition de la racine carrée par :

$$\sqrt{-1} = \img$$

Soit $x \in \setR$. Comme l'on veut conserver les propriétés des produits et puissances, on note que :

$$(x \cdot \img)^2 = x^2 \cdot \img^2 = x^2 \cdot (-1) = - x^2$$

On a par conséquent :

$$\sqrt{-x^2} = x \cdot \img$$


*** Notation

On note aussi :

$$x \img = \img x = x \cdot \img$$


*** Remarque

Attention à ne pas confondre les variables « $i$ » avec le nombre imaginaire « $\img$ ».


** Définition

Nous allons à présent nous intéresser aux propriétés algébriques des couples nombre réel - nombre imaginaire. Pour tout $(a, b) \in \setR^2$, nous introduisons la notation complexe :

$$a + \img b$$

L'ensemble des nombres complexes est simplement l'ensemble des couples réel - imaginaire :

$$\setC = \{ a + \img b : \ a,b \in \setR \}$$


** Parties réelles et imaginaires

Soit $z \in \setC$ et $a,b \in \setR$ tels que :

$$z = a + \img b$$

On nomme $a$ la partie réelle de $z$, et on la note :

$$\Re(z) = a$$

On nomme $b$ la partie imaginaire de $z$, et on la note :

$$\Im(z) = b$$

On a donc :

$$z = \Re(z) + \img \Im(z)$$


** Complexe conjugué

Soit $z \in \setC$ et $a,b \in \setR$ tels que :

$$z = a + \img b$$

Le complexe conjugué de $z$ est le nombre donné par :

$$\conjaccent{z} = \conjugue(z) = a + \img (-b) = a - \img b$$

On a donc :

#+BEGIN_CENTER
\(
\Re(\conjaccent{z}) = \Re(z) \\
\Im(\conjaccent{z}) = - \Im(z)
\)
#+END_CENTER


*** Conjugué carré

Il est clair d'après la définition que :

$$\conjugue \conjugue z = z$$


** Addition

Soient $x,y \in \setC$ et $a,b,c,d \in \setR$ tels que :

#+BEGIN_CENTER
\(
x = a + \img b \\
y = c + \img d
\)
#+END_CENTER

Comme nous désirons conserver les propriétés de commutativité et d'associativité de l'addition, on a :

$$x + y = a + \img b + c + \img d = (a + c) + \img (b + d)$$


** Soustraction

Soient $x,y \in \setC$ et $a,b,c,d \in \setR$ tels que :

#+BEGIN_CENTER
\(
x = a + \img b \\
y = c + \img d
\)
#+END_CENTER

Comme nous souhaitons conserver les propriétés de la soustraction, on a simplement :

$$x + y = a + \img b - (c + \img d) = a + \img b - c - \img d = (a - c) + \img (b - d)$$


** Multiplication

Soient $x,y \in \setC$ et $a,b,c,d \in \setR$ tels que :

#+BEGIN_CENTER
\(
x = a + \img b \\
y = c + \img d
\)
#+END_CENTER

Comme nous voulons conserver les propriétés de la multiplication, nous écrivons :

$$x \cdot y = (a + \img b) \cdot (c + \img d) = a \cdot c + \img a \cdot d + \img b \cdot c + \img^2 b  \cdot d$$

En tenant compte de la définition du nombre imaginaire, on a :

$$x \cdot y = a \cdot c + \img a \cdot d + \img b \cdot c - b  \cdot d$$

et finalement :

$$x \cdot y = (a \cdot c - b  \cdot d) + \img (a \cdot d + b \cdot c)$$

On a donc :

#+BEGIN_CENTER
\(
\Re(x \cdot y) = \Re(x) \cdot \Re(y) - \Im(x) \cdot \Im(y) \\
\Im(x \cdot y) = \Re(x) \cdot \Im(y) + \Im(x) \cdot \Re(y)
\)
#+END_CENTER


*** Mixte

On en déduit le cas particulier suivant :

$$(a + \img b) \cdot c = a \cdot c + \img b \cdot c$$

*** Multiplication par $\img$

Soit $z \in \setC$ et $a,b \in \setR$ tels que :

$$z = a + \img b$$

Comme :

$$\img z = \img a + \img^2 b = \img a - b$$

on a :

#+BEGIN_CENTER
\(
\Re(\img z) = -b = - \Im(z) \\
\Im(\img z) = a = \Re(z)
\)
#+END_CENTER


*** Conjugué

On a :

$$\conjugue(x) \cdot \conjugue(y) = (a \cdot c - b  \cdot d) - \img (a \cdot d + b \cdot c) = \conjugue(x \cdot y)$$


** Module

Soit $z \in \setC$ et $a,b \in \setR$ tels que :

$$z = a + \img b$$

On définit le module de $z$ par :

$$\abs{z} = \sqrt{a^2 + b^2} \ge 0$$

On voit que le module est un réel positif. Dans le cas d'un réel, le module s'identifie à la valeur absolue, ce qui justifie la notation identique.


*** Conjugué

On voit que :

$$z \cdot \conjaccent{z} = (a + \img b) \cdot (a - \img b) = a^2 + b^2 + \img (a \cdot b - a \cdot b) = a^2 + b^2$$

c'est-à-dire :

$$z \cdot \conjaccent{z} = \abs{z}^2$$


** Inverse

Soit $z \in \setC$ et $a,b \in \setR$ tels que :

$$z = a + \img b$$

Multiplions la relation $z \cdot \conjaccent{z} = \abs{z}^2$ par :

$$\left( z \cdot \abs{z}^2 \right)^{-1}$$

Il vient :

$$\unsur{z} = \frac{ \conjaccent{z} }{ \abs{z}^2 }$$

Nous pouvons donc évaluer l'inverse d'un nombre complexe :

$$\unsur{z} = \frac{ a - \img b }{ a^2 + b^2 } = \frac{ a }{ a^2 + b^2 } - \img \frac{ b }{ a^2 + b^2 }$$

*** Inverse de $\img$

On a en particulier :

$$\unsur{\img} = -\img$$


** Division

Soient $x,y \in \setC$ et $a,b,c,d \in \setR$ tels que :

#+BEGIN_CENTER
\(
x = a + \img b \\
y = c + \img d
\)
#+END_CENTER

On définit la division de deux nombres complexes par :

$$\frac{x}{y} = x \cdot \unsur{y} = \frac{ x \cdot \conjaccent{y} }{ \abs{y}^2 }$$

On a donc :

$$\frac{x}{y} = \frac{(a + \img b) \cdot (c - \img d)}{c^2 + d^2}$$

ou encore :

$$\frac{x}{y} = \frac{(a \cdot c + b \cdot d) + \img (b \cdot c - a \cdot d)}{c^2 + d^2}$$


** Obtention des parties réelles et imaginaires

Soit $z \in \setC$ et $a,b \in \setR$ tels que :

$$z = a + \img b$$

En additionnant $z$ et son conjugué, on obtient :

$$z + \conjaccent{z} = (a + \img b) + (a - \img b) = 2 a = 2 \Re(z)$$

ce qui permet d'obtenir une expression de la partie réelle :

$$\Re(z) = \unsur{2}(z + \conjaccent{z})$$

En soustrayant $z$ et $\conjaccent{z}$, on obtient :

$$z - \conjaccent{z} = (a + \img b) - (a - \img b) = 2 \img b = 2 \img \Im(z)$$

ce qui permet d'obtenir une expression de la partie imaginaire :

$$\Im(z) = \unsur{2\img}(z - \conjaccent{z})$$


*** Cas particuliers

On en déduit directement que si $z = \conjaccent{z}$, on a $\Im(z) = 0$ et $z\in\setR$. Par contre, si $z = -\conjaccent{z}$, on a $\Re(z) = 0$ et $z$ est purement imaginaire.


** Puissance

Soit $n \in \setN$ avec $n \ne 0$, $z \in \setC$ et $a,b \in \setR$ tels que :

$$z = a + \img b$$

On définit la $n^{ième}$ puissance d'un nombre complexe par :

#+BEGIN_CENTER
\(
z^0 = 1 \\
z^n = z \cdot z^{n-1}
\)
#+END_CENTER


*** Négatives

Les puissances négatives sont données par :

$$z^{-n} = \unsur{z^n}$$


*** Racines

La $n^{ième}$ racine $x \in \setC$ de $z$ est définie par :

$$x^n = z$$

On a alors :

$$z = x^{1/n}$$


*** Fractionnaires

Soit $m \in \setN$. on a simplement :

$$z^{m/n} = \left( z^{1/n} \right)^m$$


*** Réelles

Soit $s \in \setR$ et une suite $\{ r_i \in \setQ : i \in \setN \}$ qui converge vers $s$. On définit :

$$z^s = \lim_{i \to \infty} z^{r_i}$$


** Ordre partiel

Soit les complexes $x,y \in \setC$ et les réels $a,b,c,d \in \setR$ tels que :

#+BEGIN_CENTER
\(
x = a + \img b \\
y = c + \img d
\)
#+END_CENTER

On dit que $x$ est plus petit que $y$ au sens des composantes, et on le note $x \le y$ si les deux conditions suivantes sont vérifiées :

#+BEGIN_CENTER
\(
a \le c \\
b \le d
\)
#+END_CENTER


** Ordre total

Soit les complexes $x,y \in \setC$ et les réels $a,b,c,d \in \setR$ tels que :

#+BEGIN_CENTER
\(
x = a + \img b \\
y = c + \img d
\)
#+END_CENTER

On dit que $x$ est plus petit que $y$ et on le note $x \preceq y$ si :

$$a \strictinferieur c$$

ou si les conditions suivantes sont vérifiées :

#+BEGIN_CENTER
\(
a = c \\
b \le d
\)
#+END_CENTER


** Inclusion

Pour tout $a \in \setR$, on a $a = a + \img 0 \in \setC$. On considère donc que $\setR \subseteq \setC$.


* Intervalles de réels

#+TOC: headlines 1 local

\label{chap:intervallesDeReels}


** Dépendances

  - Chapitre \ref{chap:reel} : Les nombres réels


** Intervalles bornés

Soit $a,b \in \setR$ avec $a \le b$. L'intervalle fermé $[a,b]$ est défini par :

$$[a,b] = \{ x \in \setR : a \le x \le b \}$$

L'intervalle ouvert $\intervalleouvert{a}{b}$ est défini par :

$$\intervalleouvert{a}{b} \ = \{ x \in \setR : a \strictinferieur x \strictinferieur b \}$$

On recontre aussi les intervalles semi-ouverts à gauche :

$$\intervallesemiouvertgauche{a}{b} = \{ x \in \setR : a \strictinferieur x \le b \}$$

ou à droite :

$$\intervallesemiouvertdroite{a}{b} \ = \{ x \in \setR : a \le x \strictinferieur b \}$$


** Intervalles non bornés

Soit $a \in \setR$. Les intervalles non bornés sont des intervalles partant de l'infini négatif ou/et allant jusqu'à l'infini positif :

\begin{align}\relax
\intervallesemiouvertdroite{a}{+\infty} &= \{ x \in \setR : x \ge a \} \\
\intervalleouvert{a}{+\infty} &= \{ x \in \setR : x \strictsuperieur a \} \\
\intervallesemiouvertgauche{-\infty}{a} &= \{ x \in \setR : x \le a \} \\
\intervalleouvert{-\infty}{a} &= \{ x \in \setR : x \strictinferieur a \} \\
\intervalleouvert{-\infty}{+\infty} &= \setR
\end{align}


** Boules

Soit $c,r,x \in \setR$ avec $r \ge 0$. La condition $\abs{x - c} \le r$ est équivalente à $x - c \le r$ et $-(x - c) = c - x \le r$. On en déduit que :

#+BEGIN_CENTER
\(
x \le c + r \\
x \ge c - r
\)
#+END_CENTER

ce qui revient à dire que $x \in [c - r, c + r]$. Nous obtenons un résultat analogue avec les inégalités strictes. On peut donc exprimer les boules en terme d'intervalles :

\begin{align}
\boule[c,r] &= [c - r, c + r] \\
\boule(c,r) &= \ \intervalleouvert{c - r}{c + r}
\end{align}

On peut inverser ces relations et exprimer certains intervalles en termes de boules. Soit $a = c - r$ et $b = c + r$. On a alors $a \le b$ et :

#+BEGIN_CENTER
\(
a + b = c - r + c + r = 2 c \\
b - a = c + r - c + r = 2 r
\)
#+END_CENTER

On en déduit que :

\begin{align}\relax
[a,b] &= \boule\left[\frac{a + b}{2},\frac{b - a}{2}\right] \\ \\
\intervalleouvert{a}{b} &= \boule\left(\frac{a + b}{2},\frac{b - a}{2}\right)
\end{align}


** Majorants et minorants

Soit $a,b \in \setR$ avec $a \le b$ et $x \in \setR$.

Pour que $x \ge [a,b]$, il faut que $x \ge b$. Si $y \in [a,b]$, on aura alors $x \ge b \ge y$ d'où $x \ge y$ par transitivité de l'ordre. On en déduit que :

$$\major [a,b] = \intervallesemiouvertgauche{b}{+\infty}$$

On obtient le même résultat pour l'intervalle ouvert :

$$\major \intervalleouvert{a}{b} = \intervallesemiouvertgauche{b}{+\infty}$$

ainsi que pour les intervalles semi-ouverts.

Pour que $x \le [a,b]$, il faut que $x \le a$. Si $y \in [a,b]$, on aura alors $x \le a \le y$ d'où $x \le y$ par transitivité de l'ordre. On en déduit que :

$$\minor [a,b] = \intervallesemiouvertdroite{-\infty}{a}$$

On obtient le même résultat pour l'intervalle ouvert :

$$\minor \intervalleouvert{a}{b} = \intervallesemiouvertdroite{-\infty}{a}$$

ainsi que pour les intervalles semi-ouverts.


** Maximum et minimum

Soit $a,b \in \setR$ avec $a \le b$. On a clairement :

\begin{align}
\max [a,b] &= \max \intervallesemiouvertgauche{a}{b} = \max \intervallesemiouvertgauche{-\infty}{b} = b \\
\min [a,b] &= \min \intervallesemiouvertdroite{a}{b} = \min \intervallesemiouvertdroite{a}{+\infty} \ = a
\end{align}

Les autre types d'intervalles n'admettent ni maximum ni minimum.


** Supremum et infimum

Dans le cas où le maximum de l'intervalle $I$ existe, on a $\sup I = \max I$. Dans les autres cas, on a par exemple :

$$\sup \intervalleouvert{a}{b} = \min \major \intervalleouvert{a}{b} = \min [b, +\infty[ \ = b$$

et le même résultat pour les intervalles semi-ouverts.

Dans le cas où le minimum de l'intervalle $I$ existe, on a $\inf I = \min I$. Dans les autres cas, on a par exemple :

$$\inf \intervalleouvert{a}{b} = \max \minor \intervalleouvert{a}{b} = \max \ ]-\infty, a] = a$$

et le même résultat pour les intervalles semi-ouverts.


** Adhérence, intérieur et frontière

On peut vérifier que :

\begin{align}
\adh [a,b] &= \adh \intervalleouvert{a}{b} = [a,b] \\
\interieur [a,b] &= \interieur \intervalleouvert{a}{b} = \ ]a,b[
\end{align}

et ainsi de suite. La frontière est donc simplement :

$$\partial [a,b] = \partial \intervalleouvert{a}{b} = \{a,b\}$$


* Dimension $n$

#+TOC: headlines 1 local

\label{chap:dimensionN}


** Dépendances

  - Chapitre \ref{chap:naturel} : Les naturels
  - Chapitre \ref{chap:entier} : Les entiers
  - Chapitre \ref{chap:rationel} : Les rationnels
  - Chapitre \ref{chap:reel} : Les réels
  - Chapitre \ref{chap:complexe} : Les complexes


** Définition

Conformément à la définition de la « puissance » d'un ensemble, $\setN^n$ est l'ensemble des $n$-tuples :

$$\setN^n = \{ (m_1,m_2,...,m_n) : m_1,m_2,...,m_n \in \setN \}$$

où chaque composante $m_i$ est un naturel. On a pareillement :

#+BEGIN_CENTER
\(
\setZ^n = \{ (z_1,z_2,...,z_n) : z_1,z_2,...,z_n \in \setN \} \\
\setQ^n = \{ (q_1,q_2,...,q_n) : q_1,q_2,...,q_n \in \setQ \} \\
\setR^n = \{ (r_1,r_2,...,r_n) : r_1,r_2,...,r_n \in \setR \} \\
\setC^n = \{ (c_1,c_2,...,c_n) : c_1,c_2,...,c_n \in \setC \}
\)
#+END_CENTER

Dans la suite, nous notons :

$$\mathcal{D} = \{ \setN^n , \setZ^n , \setQ^n , \setR^n , \setC^n \}$$

On dit que les éléments de $\mathcal{D}$ sont des ensembles de dimension $n$.


** Egalité

Soit l'ensemble $A^n \in \mathcal{D}$ et $x,y \in A^n$. On a :

#+BEGIN_CENTER
\(
x = (x_1,x_2,...,x_n) \\
y = (y_1,y_2,...,y_n)
\)
#+END_CENTER

On dit que $x = y$ si et seulement si $x_i = y_i$ pour tout $i \in \{1,2,...,n\}$.

De même, on dit que $x = 0$ si et seulement si $x_i = 0$ pour tout $i \in \{1,2,...,n\}$.


** Ordre partiel

Soit l'ensemble $A^n \in \mathcal{D}$ et $x,y \in A^n$. On a :

#+BEGIN_CENTER
\(
x = (x_1,x_2,...,x_n) \\
y = (y_1,y_2,...,y_n)
\)
#+END_CENTER

On dit que $x \le y$ si et seulement si $x_i \le y_i$ pour tout $i \in \{1,2,...,n\}$. On le note aussi $y \ge x$.

On dit que $x \strictinferieur y$ si et seulement si $x_i \strictinferieur y_i$ pour tout $i \in \{1,2,...,n\}$. On le note aussi $y \strictsuperieur x$.


** Opérations internes

Soit l'ensemble $A^n \in \mathcal{D}$ et $x,y \in A^n$. On a :

#+BEGIN_CENTER
\(
x = (x_1,x_2,...,x_n) \\
y = (y_1,y_2,...,y_n)
\)
#+END_CENTER

pour certains $x_i,y_i \in A$.

Les opérations sur $A^n$ sont définies par la même opération appliquée aux composantes. L'addition s'écrit donc :

$$x + y = (x_1 + y_1 , x_2 + y_2 , ... , x_n + y_n)$$

tandis que la soustraction est donnée par :

$$x - y = (x_1 - y_1 , x_2 - y_2 , ... , x_n - y_n)$$


** Multiplication mixte

La multiplication mixte $\cdot : A \times A^n \to A^n$ est définie par :

$$\alpha \cdot x = (\alpha \cdot x_1 , \alpha \cdot x_2 , ... , \alpha \cdot x_n)$$

pour tout $\alpha \in A$.


*** Notations

On note bien entendu :

$$x \cdot \alpha = \alpha \cdot x$$

Choisissant un $\beta \in A$, on pose également :

$$\alpha \cdot \beta \cdot x = (\alpha \cdot \beta) \cdot x$$

Enfin, le signe de multiplication est souvent omis :

$$\alpha \ x = \alpha \cdot x$$


** Neutre

Soit l'ensemble $A^n \in \mathcal{D}$. On note :

$$0 = (0,0,...,0)$$

l'élément $0 \in A^n$


** Lien avec les fonctions

On voit clairement que ces relations et opérations sont équivalentes aux mêmes relations et opérations définies sur les fonctions :

$$\{1,2,...,n\} \mapsto \{f(1),f(2),...,f(n)\} \equiv (f_1,f_2,...,f_n)$$

associées.


** Puissance

Soit $x = (x_1,...x_n) , y = (y_1,...,y_n) \in A^n$. La puissance s'écrit :

$$y^x = y_1^{x_1} \cdot y_2^{x_2} \cdot ... \cdot y_n^{x_n}$$


** Factorielle

Soit $m = (m_1,m_2,...,m_n) \in \setN^n$. On définit :

$$m ! = m_1 ! \cdot m_2 ! \cdot ... \cdot m_n !$$


* Matrices

#+TOC: headlines 1 local

\label{chap:matrices}


** Dépendances

  - Chapitre \ref{chap:reel} : Les réels
  - Chapitre \ref{chap:complexe} : Les complexes


** Définition

Les matrices sont des tableaux de réels, de complexes, ou de composantes d'une autre nature appartenant à un corps quelconque. Voici un exemple d'une matrice à $m$ lignes et $n$ colonnes :

#+BEGIN_CENTER
\(
A =
\begin{Matrix}{cccc}
a_{11} & a_{12} & \ldots & a_{1n} \\
a_{21} & a_{22} & \ldots & a_{2n} \\
\vdots &        & \ddots &  \vdots \\
a_{m1} & a_{m2} & \ldots & a_{mn}
\end{Matrix}
\)
#+END_CENTER

où les $a_{ij}$ appartiennent à un corps $\corps$. On a également des versions simplifiées de cette notation :

$$A = ( a_{ij} )_{i,j} = [ a_{ij} ]_{i,j}$$

Par convention, le numéro de ligne passe avant le numéro de colonne à l'extérieur de la parenthèse (ou du crochet).

On note $\matrice(\corps,m,n)$ l'ensemble des matrices à $m$ lignes et $n$ colonnes. On dit d'une matrice $A \in \matrice(\corps,m,n)$ qu'elle est de taille $(m,n)$.


** Composantes

Soit $A = (a_{ij})_{i,j}$. La fonction $\composante_{ij}$ donne la composante située à la $i^{ème}$ ligne et à la $j^{ème}$ colonne :

$$a_{ij} = \composante_{ij} A$$


** Blocs

Soit $A = (a_{ij})_{i,j}$. La fonction $\bloc_{ijkl}$ donne la sous-matrice située entre la $i^{ème}$ et la $j^{ème}$ ligne, ainsi qu'entre la $k^{ième}$ et la $l^{ième}$ colonne :

#+BEGIN_CENTER
\(
\bloc_{ijkl} A =
\begin{Matrix}{cccc}
a_{ik} & a_{i,k+1} & \ldots & a_{il} \\
a_{i+1,k} & a_{i+1,k+1} & \ldots & a_{i+1,l} \\
\vdots &        & \ddots &  \vdots \\
a_{jk} & a_{j,k+1} & \ldots & a_{jl}
\end{Matrix}
\)
#+END_CENTER

On appelle bloc une telle sous-matrice.


** Partitionnement en blocs

Il est parfois très utile de partitionner une matrice en blocs, ou de former une matrice plus grande à partir de matrices de tailles plus petites. On note alors :

#+BEGIN_CENTER
\(
A =
\begin{Matrix}{cc}
B & C \\
D & E
\end{Matrix}
=
\begin{Matrix}{cccccc}
b_{11} & \ldots & b_{1n} & c_{11} & \ldots & c_{1p} \\
\vdots &        & \vdots & \vdots &        & \vdots \\
b_{m1} & \ldots & b_{mn} & c_{m1} & \ldots & c_{mp} \\
d_{11} & \ldots & d_{1n} & e_{11} & \ldots & e_{1p} \\
\vdots &        & \vdots & \vdots &       & \vdots \\
d_{r1} & \ldots & d_{rn} & e_{r1} & \ldots & e_{rp}
\end{Matrix}
\)
#+END_CENTER

où les $b_{ij},c_{ij},d_{ij},e_{ij}$ sont respectivement les composantes des matrices $B,C,D,E$. Ces matrices doivent évidemment être de tailles compatibles (nombres de lignes identiques pour $B$ et $C$, ainsi que pour $D$ et $E$ ; nombres de colonnes identiques pour $B$ et $D$, ainsi que pour $C$ et $E$).


** Formes lignes et colonnes

On peut exprimer une matrice $A \in \matrice(\corps,m,n)$ sous la forme de lignes superposées. Soit :

#+BEGIN_CENTER
\(
A =
\begin{Matrix}{c}
l_1 \\
l_2 \\
\vdots \\
l_m
\end{Matrix}
\)
#+END_CENTER

où les $l_i \in \matrice(\corps,1,n)$ sont les lignes de $A$. On note :

$$\ligne_i A = l_i$$

On peut aussi l'exprimer sous la forme de colonnes juxtaposées. Soit :

$$A = [c_1 \ c_2 \hdots \ c_n]$$

où les $c_i \in \matrice(\corps,m,1)$ sont les colonnes de $A$. On note :

$$\colonne_i A = c_i$$


** Transposée

Transposer une matrice $A = (a_{ij})_{i,j} \in \matrice(\corps,m,n)$ consiste à agencer ses lignes en colonnes et vice-versa :

#+BEGIN_CENTER
\(
A^T =
\begin{Matrix}{cccc}
a_{11} & a_{21} & \ldots & a_{m1} \\
a_{12} & a_{22} & \ldots & a_{m2} \\
\vdots &        & \ddots &  \vdots \\
a_{1n} & a_{2n} & \ldots & a_{mn}
\end{Matrix} = (a_{ji})_{i,j}
\)
#+END_CENTER

On voit que la transposée $A^T \in \matrice(\corps,n,m)$.

On a clairement :

$$\big( A^T \big)^T = A$$


** Vecteurs lignes et colonnes

Les vecteurs ligne sont des matrices ne possédant qu'une seule ligne. Ils sont donc de taille générique $(1,n)$. Les vecteurs colonne sont des matrices ne possédant qu'une seule colonne. Ils sont donc de taille générique $(m,1)$.

Les vecteurs {\em colonne} sont notés :

$$x = (x_i)_i$$

On a aussi :

$$x_i = \composante_i x$$

*** Equivalence avec $\corps^n$

A partir d'un $n$-tuple $(x_1,x_2,...,x_n) \in \corps^n$, on peut former un vecteur ligne $x \in \matrice(\corps,1,n)$ par :

$$x = [x_1 \ x_2 \ \hdots \ x_n]$$

et un vecteur colonne $x' \in \matrice(\corps,n,1)$ par :

#+BEGIN_CENTER
\(
x' =
\begin{Matrix}{c}
x_1 \\
x_2 \\
\vdots \\
x_n
\end{Matrix}
=
[x_1 \ x_2 \ \hdots \ x_n]^T
\)
#+END_CENTER

On a donc $\matrice(\corps,1,n),\matrice(\corps,n,1) \equiv \corps^n$.


*** Notation

Sauf mention contraire, nous convenons dans la suite que tout vecteur $u \in  \corps^n$ sera associé à un vecteur {\em colonne} de même nom $u \in \matrice(\corps,n,1)$. Si nous voulons utiliser un vecteur {\em ligne}, nous le noterons alors $u^T$ (ou $u^\dual$ pour des raisons qui apparaîtront plus loin).


** Carrées et rectangulaires

Les matrices $A \in \matrice(\corps,n,n)$ ayant même nombre de colonnes et de lignes sont dites « carrées ». Les matrices $A \in \matrice(\corps,m,n)$ avec $m \ne n$ sont dites « rectangulaires ». Lorsque $m \strictinferieur n$, on dit que la matrice est « longue ». Si $m \strictsuperieur n$, on dit que la matrice est « haute ».


** Matrices diagonales

Une matrice diagonale contient des composantes nulles partout, à l'exception de la diagonale où aucune contrainte n'est fixée. On peut représenter une matrice diagonale $D \in \matrice(\corps,m,n)$ par :

$$D = ( d_i \cdot \indicatrice_{ij} )_{i,j}$$

On peut aussi former une matrice diagonale de taille $(m,n)$ à partir d'un vecteur $d = (d_i)_i$ de taille $(p,1)$, où $p \le \min \{ m , n \}$. On le note :

$$D = \diagonale_{m,n}(d) = \diagonale_{m,n}(d_1,...,d_p)$$

où :

#+BEGIN_CENTER
\(
\composante_{ij} \diagonale_{m,n}(d_1,...,d_p) =
\begin{cases}
d_i & \text{ si } i = j \in \{1,2,...,p\} \\
0 & \text{ sinon}
\end{cases}
\)
#+END_CENTER


*** Carrée

Dans le cas particulier où $m = n$, on note aussi :

$$\diagonale_n(d) = \diagonale_{n,n}(d)$$


** Matrices triangulaires

Les composantes des matrices triangulaires supérieures ne peuvent être non nulles qu'au-dessus de la diagonale, c'est-à-dire lorsque $i \le j$. Elles peuvent se représenter par :

$$T = (t_{ij} \cdot \tau^+_{ij})_{i,j}$$

où :

#+BEGIN_CENTER
\(
\tau^+_{ij} =
\begin{cases}
1 & \mbox{ si } i \le j \\
0 & \mbox{ si } i \strictsuperieur j
\end{cases}
\)
#+END_CENTER

Les composantes des matrices triangulaires inférieures ne peuvent être non nulles qu'au-dessous de la diagonale, c'est-à-dire lorsque $i \ge j$. Elles peuvent se représenter par :

$$T = (t_{ij} \cdot \tau^-_{ij})_{i,j}$$

où :

#+BEGIN_CENTER
\(
\tau^-_{ij} =
\begin{cases}
1 & \mbox{ si } i \ge j \\
0 & \mbox{ si } i \strictinferieur j
\end{cases}
\)
#+END_CENTER


** Egalité

Il est clair que deux matrices $A,B \in \matrice(\corps,m,n)$ :

#+BEGIN_CENTER
\(
A = ( a_{ij} )_{i,j} \\
B = ( b_{ij} )_{i,j}
\)
#+END_CENTER

sont égales ($A = B$) si tous leurs éléments le sont :

$$a_{ij} = b_{ij}$$

pour tout $(i,j) \in \{ 1,2,...,m \} \times \{ 1,2,...,n \}$.


** Ordre partiel

Soit les matrices $A,B \in \matrice(\corps,m,n)$ :

#+BEGIN_CENTER
\(
A = ( a_{ij} )_{i,j} \\
B = ( b_{ij} )_{i,j}
\)
#+END_CENTER

On dit que $A$ est inférieure à $B$, et on le note $A \le B$, si :

$$a_{ij} \le b_{ij}$$

pour tout $(i,j) \in \{ 1,2,...,m \} \times \{ 1,2,...,n \}$.


** Addition et soustraction

Soit les matrices de tailles identiques $A, B \in \matrice(\corps,m,n)$ :

#+BEGIN_CENTER
\(
A = (a_{ij})_{i,j} \\
B = (b_{ij})_{i,j}
\)
#+END_CENTER

L'addition et la soustraction des matrices sont simplement définies par l'addition et la soustraction des composantes :

#+BEGIN_CENTER
\(
A + B = (a_{ij} + b_{ij})_{i,j} \\
A - B = (a_{ij} - b_{ij})_{i,j}
\)
#+END_CENTER

On a donc clairement :

$$A + B = B + A$$


*** Neutre

La matrice nulle $0$ est le neutre pour cette opération :

$$A + 0 = A$$

On en déduit que tous ses éléments doivent être nuls :

$$0 = ( 0 )_{i,j}$$

On note $0_{m,n}$ au lieu de $0$ lorsqu'on veut préciser que $0$ est
de taille $(m,n)$.


*** Opposé

L'opposé de $A$, noté $-A$, est tel que :

$$A + (-A) = 0$$

On a donc clairement :

$$-A = (-a_{ij})_{i,j}$$


** Multiplication mixte

On définit la multiplication mixte $\cdot : \corps \times \matrice(\corps,m,n) \to \matrice(\corps,m,n)$ par :

$$\beta \cdot A = (\beta \cdot a_{ij})_{i,j}$$

où $\beta \in \corps$ et $A \in \matrice(\corps,m,n)$.

Choissons également $\gamma \in \corps$. On note comme d'habitude :

#+BEGIN_CENTER
\(
A \cdot \beta = \beta \cdot A \\
\gamma \cdot \beta \cdot A = (\gamma \cdot \beta) \cdot A \\
\beta A = \beta \cdot A
\)
#+END_CENTER


** Conjuguée

La conjuguée d'une matrice $A = (a_{ij})_{i,j} \in \matrice(\setC,m,n)$ est définie par la conjuguée des composantes :

$$\conjugue A = \bar{A} = ( \bar{a}_{ij} )_{i,j} = ( \conjugue a_{ij} )_{i,j}$$


* Progressions matricielles

#+TOC: headlines 1 local

\label{chap:progressionsMatricielles}


** Géométrique

Soit une matrice carréé $A \in \matrice(\corps, n, n)$.

$$G_N = \sum_{k = 0}^N A^k = I + A + A^2 + ... + A^N$$


*** Forme gauche

$$G_N = I + A \cdot (I + A + ... + A^N) - A^{N + 1}$$

$$G_N = I + A \cdot G_N - A^{N + 1}$$

$$(I - A) \cdot G_N = I - A^{N + 1}$$

$$(I - A) \cdot \sum_{k = 0}^N A^k = I - A^{N + 1}$$


*** Forme droite

$$G_N = I + (I + A + ... + A^N) \cdot A - A^{N + 1}$$

$$G_N = I + G_N \cdot A - A^{N + 1}$$

$$G_N \cdot (I - A) = I - A^{N + 1}$$

$$\sum_{k = 0}^N A^k \cdot (I - A) = I - A^{N + 1}$$


*** Inversible

$$\sum_{k = 0}^N A^k = (I - A)^{-1} \cdot (I - A^{N + 1})$$

$$\sum_{k = 0}^N A^k = (I - A^{N + 1}) \cdot (I - A)^{-1}$$


* Polynômes

#+TOC: headlines 1 local

\label{chap:polynomes}


** Dépendances

  - Chapitre \ref{chap:reel} : Les réels
  - Chapitre \ref{chap:complexe} : Les complexes
  - Chapitre \ref{chap:somme} : Les sommes
  - Chapitre \ref{chap:produit} : Les produits


** Définition

Soit un corps $\corps$. On dit qu'une fonction $p : \corps \mapsto \corps$ est un polynôme de degré $n$ si on peut trouver des éléments $a_0,a_1,...a_n \in \corps$ tels que :

$$p(x) = \sum_{i = 0}^n a_i \cdot x^i$$

pour tout $x \in \corps$. On note $\polynome(\corps,n)$ l'espace des polynômes de degré $n$ définis sur $\corps$.

Un cas particulier important est celui des polynômes réels :

$$\mathcal{P}_n = \polynome(\setR,n)$$


** Monôme

Un monôme $\mu_i : \corps \mapsto \corps$ est une fonction de la forme :

$$\mu_i(x) = x^i$$

pour tout $x \in \corps$.


** Racines

On dit que $r$ est une racine du polynôme $p$ si :

$$p(r) = 0$$

Nous allons montrer que tout polynôme de degré $n$ non nul
admet au plus $n$ racines distinctes.

Ce qui revient à dire que si un polynôme de degré $n$ admet $m+1$ racines distinctes avec $m \ge n$, alors ce polynôme est forcément nul.

Soit $n = 0$. Considérons un polynôme $p_0$ défini par $p_0(x) = a_0$ pour un certain $a_0 \in \corps$. Comme $n + 1 = 1$, on peut trouver au moins un $x \in \corps$ tel que $p_0(x_0) = a_0 = 0$. On en conclut que $a_0 = 0$ et que $p_0(x) = 0$ pour tout $x \in \corps$, ce qui revient à dire que $p_0 = 0$. La thèse est donc vérifiée pour $n = 0$.

A présent, supposons que l'affirmation soit vraie pour $n - 1$. Choisissons un polynôme $p_n$ de degré $n$ :

$$p_n(x) = \sum_{i=0}^n a_i \cdot x^i$$

Supposons que $p$ possède $m+1$ racines

$$p_n(x_0) = p_n(x_1) = ... = p_n(x_m) = 0$$

avec $m \ge n$ et  :

$$x_0 \strictinferieur x_1 \strictinferieur ... \strictinferieur x_m$$

On a alors :

$$p_n(x) = p_n(x) - p_n(x_0) = \sum_{i=1}^n a_i \cdot (x^i - x_0^i)$$

Mais les propriétés de factorisation des progressions géométriques (voir la section \ref{sec:factorisation_progression_geometrique}) nous permettent d'affirmer que :

$$x^i - x_0^i = (x - x_0) \sum_{j = 0}^{i - 1} x^j \cdot x_0^{i - 1 -j}$$

on en déduit que :

$$p_n(x) = (x - x_0) \cdot q_{n-1}(x)$$

où le polynôme $q_{n - 1}$ est défini par :

$$q_{n - 1}(x) = \sum_{i = 1}^n a_i \sum_{j = 0}^{i - 1}  x^j \cdot x_0^{i - 1 - j}$$

Cette expression ne faisant intervenir que les puissances $1,x,...,x^{n-1}$, on voit que $q_{n-1} \in \mathcal{P}_n$ est de degré $n-1$. Considérons les cas des racines $x = x_1, x_2, ...,x_m$ de $p_n$. On a :

$$0 = p_n(x_k) = (x_k - x_0) \cdot q_{n-1}(x_k)$$

Comme $x_k - x_0 \ne 0$, on peut multiplier par $(x_k - x_0)^{-1}$ pour obtenir :

$$q_{n-1}(x_k) = \frac{p_{n-1}(x_k)}{x_k - x_0} = 0$$

Le polynôme $q_{n-1}$ possède par conséquent au moins $m-1 \ge n-1$ racines distinctes. Il est dès lors nul par l'hypothèse de récurrence. La factorisation de $p_n$ devient alors :

$$p_n(x) = (x - x_0) \cdot q_{n-1}(x) = 0$$

pour tout $x \in \corps$. Le polynôme $p_n$ est également nul et la thèse est démontrée.


*** Egalité

Si deux polynômes $p_1,p_2 \in \mathcal{P}_n$ sont égaux en $m + 1 \ge n + 1$ points distincts, alors le polynôme de degré $n$ :

$$h = p_1 - p_2$$

admet $m+1$ racines. Il est donc nul et $p_1 = p_2$.


** Factorisation

Choisissons un polynôme $p\in\mathcal{P}_n$ qui admet $n$ racines
distinctes $x_1,x_2,...,x_n$. Soit alors $x_0 \notin \{x_1,...,x_n\}$
et :

$$A = \frac{p(x_0)}{\prod_{i = 1}^n (x_0 - x_i)}$$

On voit que le polynôme :

$$q(x) = A \cdot \prod_{i=1}^n (x - x_i)$$

est égal à $p$ en chacune des racines :

$$p(x_i) = q(x_i) = 0 \qquad i = 1,2,...,n$$

Mais par la définition de $A$, on a aussi :

$$p(x_0) = q(x_0)$$

Les deux polynômes sont donc égaux en $n+1$ points distincts.
Comme ils sont tous deux de degré $n$, on en déduit que $p = q$.
Les coefficients doivent donc tous être égaux, et comme :

#+BEGIN_CENTER
\(
p(x) = a_n \cdot x^n + \sum_{i = 0}^{n - 1} a_i \cdot x^i \\
q(x) = A \cdot x^n + \sum_{i = 0}^{n - 1} b_i(x_1,...,x_n) \cdot x^i
\)
#+END_CENTER

on a $A = a_n$ et :

$$p(x) = a_n \cdot \prod_{i=1}^n (x - x_i)$$


** Binômes canoniques

Le binôme canonique de degré $n \in \setN$ est une fonction $b_n : \corps \to \corps$ définie par :

$$b_n(x) = (1 + x)^n$$

pour tout $x \in \corps$. On a par exemple :

\begin{align}
b_0(x) &= (1 + x)^0 = 1 \\
b_1(x) &= (1 + x)^1 = 1 + x \\
b_2(x) &= (1 + x)^2 = (1 + x) \cdot (1 + x) = 1 + 2 \ x + x^2
\end{align}

On voit donc que le binôme canonique de degré $n$ peut s'écrire comme :

$$b_n(x) = (1 + x)^n = \sum_{k = 0}^n a_{nk} \cdot x^k$$

pour certains coefficients $a_{nk} \in \corps$. On nomme ces coefficients les « nombres binômiaux », et on les note :

$$\binome{n}{k} = a_{nk}$$

L'expression de $b_0$ nous donne :

$$\binome{0}{0} = 1$$

On peut évaluer récursivement les nombres binômiaux d'ordres plus élevés en utilisant la définition de la puissance :

$$b_n(x) = (1 + x) \cdot b_{n-1}(x)$$

Il vient alors :

\begin{align}
\sum_{k = 0}^n \binome{n}{k} \cdot x^k &= (1 + x) \sum_{k = 0}^{n - 1} \binome{n - 1}{k} \cdot x^k \\
&= \sum_{k = 0}^{n - 1} \binome{n - 1}{k} \cdot x^k + \sum_{k = 0}^{n - 1} \binome{n - 1}{k} \cdot x^{k + 1} \\
&= \sum_{k = 0}^{n - 1} \binome{n - 1}{k} \cdot x^k + \sum_{i = 1}^n \binome{n - 1}{i - 1} \cdot x^i \\
\end{align}

et finalement :

#+BEGIN_CENTER
\(
\sum_{k = 0}^n \binome{n}{k} \cdot x^k = \binome{n - 1}{0} + \sum_{k = 1}^{n - 1} \left[ \binome{n - 1}{k} + \binome{n - 1}{k - 1} \right] \cdot x^k \\
\qquad \qquad \qquad + \binome{n - 1}{n - 1} \cdot x^n
\end{Eqts*}

En égalisant les coefficients des $x^0 = 1$, nous avons :

$$\binome{n}{0} = \binome{n - 1}{0}$$

On en déduit par récurrence que :

$$\binome{n}{0} = 1$$

En égalisant les coefficients des $x^n$, nous avons :

$$\binome{n}{n} = \binome{n - 1}{n - 1}$$

On en déduit par récurrence que :

$$\binome{n}{n} = 1$$

En égalisant les coefficients de $x^k$ pour $k \in \{1,...,n-1\}$, il vient :

$$\binome{n}{k} = \binome{n - 1}{k} + \binome{n - 1}{k - 1}$$

Il est donc facile d'évaluer les coefficients de $b_n$ à partir des coefficients de $b_{n - 1}$.


** Binômes génériques

Le binôme générique de degré $n \in \setN$ est une fonction $B_n : \corps \times \corps
\mapsto \corps$ définie par :

$$B_n(x,y) = (x + y)^n$$

pour tout $(x,y) \in \corps^2$. Nous avons la forme équivalente :

$$B_n(x,y) = y^n \cdot \left( 1 + \frac{x}{y} \right)^n = y^n \cdot b_n\left( \frac{x}{y} \right)$$

c'est-à-dire :

$$B_n(x,y) = \sum_{k = 0}^n \binome{n}{k} \cdot x^k \cdot y^{n - k}$$


** Second degré

Le binôme du second degré est couramment utilisé :

$$(x + y)^2 = x \ (x + y) + y \ (x + y) = x^2 + 2 \ x \ y + y^2$$


** Symétrie

Par commutativité de l'addition, on a $B_n(x,y) = B_n(y,x)$ et :

$$\sum_{k = 0}^n \binome{n}{k} \cdot x^k \cdot y^{n - k} = \sum_{i = 0}^n \binome{n}{i} \cdot y^i \cdot x^{n - i}$$

Procédant au changement d'indice $n - i = k$, il vient :

$$\sum_{k = 0}^n \binome{n}{k} \cdot x^k \cdot y^{n - k} = \sum_{k = 0}^n \binome{n}{n - k} \cdot x^k \cdot y^{n - k}$$

On en déduit en égalisant les coefficients de $x^k$ que :

$$\binome{n}{k} = \binome{n}{n - k}$$


** Cas particuliers

En considérant le cas particuliers $x = y = 1$, on constate que :

$$\sum_{k=0}^{n} \binome{n}{k} = (1 + 1)^n = 2^n$$

Pour $x = -1$, $y = 1$, on a :

$$\sum_{k=0}^{n} \binome{n}{k} \cdot (-1)^k = (-1 + 1)^n = 0^n = 0$$

Lorsque $y = 1 - x$ on arrive à :

$$\sum_{k=0}^{n} \binome{n}{k} \cdot x^k \cdot (1-x)^{n-k} = (x + 1 - x)^n = 1^n = 1$$


** Bernstein

Soit $i,n \in \setN$ avec $i \le n$. Les polynômes de Bernstein $B_i^n$ sont définis par :

$$B_i^n(t) = \binome{n}{i} \cdot t^i \cdot (1 - t)^{n-i}$$

pour tout $t \in [0,1]$.

Considérons l'espace fonctionnel $\mathcal{F} = \fonction([0,1],\corps)$. L'opérateur de Bernstein $\mathcal{B}_n : \mathcal{F} \mapsto \mathcal{F}$ est défini par :

$$\mathcal{B}_n(f)(t) = \sum_{i = 0}^n f(i / n) \cdot B_i^n(t)$$

pour tout $f \in \mathcal{F}$ et pour tout $t \in [0,1]$.

Soit la fonction constante $c \in \mathcal{F}$ associée à un certain $c \in \corps$ et définie par :

$$c(t) = c$$

pour tout $t \in [0,1]$.

L'opérateur de Bernstein possède l'importante propriété de conserver ces fonctions constantes :

$$\mathcal{B}_n(c)(t) = \sum_{i = 0}^n c \cdot B_i^n(t) = c \sum_{i = 0}^n \binome{n}{i} t^i \cdot (1 - t)^{n-i} = c$$

quelle que soit la valeur de $t \in [0,1]$.


** Division Euclidienne

Soit deux polynomes $a \in \mathcal{P}_n$ et $b \in \mathcal{P}_m$
avec $m \le n$ et :

#+BEGIN_CENTER
\(
a(x) = \sum_{i = 0}^n a_i \cdot x^i \\
b(x) = \sum_{i = 0}^m b_i \cdot x^i
\)
#+END_CENTER

On dit que $q$ et $r$ sont respectivement le quotient et le reste
de la division euclidienne de $a$ et $b$, et on note :

$$(q,r) = \division(a,b)$$

si :

#+BEGIN_CENTER
\(
a(x) = b(x) \cdot q(x) + r(x) \\
q(x) = \sum_{i=0}^{n-m} q_i x^i \\
r(x) = \sum_{i=0}^{p} r_i x^i
\)
#+END_CENTER

avec $p \strictinferieur m \le n$.
