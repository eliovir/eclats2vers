
#+STARTUP: showall

#+TITLE: Eclats de vers : Bien-être 03 : Sport
#+AUTHOR: chimay
#+EMAIL: or du val chez gé courriel commercial
#+LANGUAGE: fr
#+LINK_HOME: file:../index.html
#+LINK_UP: file:index.html
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../style/defaut.css" />

#+OPTIONS: H:6
#+OPTIONS: toc:nil

#+TAGS: noexport(n) systema(s) mma(m)

[[file:index.org][Index des Grimoires]]

#+INCLUDE: "../include/navigan-1.org"

#+TOC: headlines 1

* Pompes

#+TOC: headlines 1 local


** Classique

  - pieds et mains au sol
    + ventre vers le sol
    + jambes tendues
  - mains au sol au niveau des épaules, bras tendus, mains vers l’avant
  - dos bien aligné
  - plier les bras, laisser le corps se rapprocher du sol
  - tendre les bras, éloigner le corps du sol
  - recommencer plusieurs fois


** En arc

  - se placer en position initiale de pompe
  - écarter un peu plus les mains
  - rapprocher le buste de la main gauche
  - faire une pompe
    + le bras gauche va naturellement pousser plus fort que le droit
  - rapprocher le buste de la main droite
  - faire une pompe
    + le bras droit va naturellement pousser plus fort que le gauche
  - recommencer plusieurs fois


** Rotative

Variante de la pompe en arc qui fait tourner le corps.

  - se placer en position initiale de pompe
  - écarter un peu plus les mains
  - rapprocher le buste de la main gauche en restant en haut
  - laisser descendre en restant à gauche
  - rapprocher le buste de la main droite en restant en bas
  - remonter en restant à droite
  - rapprocher le buste de la main gauche en restant en haut
  - recommencer plusieurs fois

Recommencer en changeant le sens de rotation


** Diamant

  - se placer en position initiale de pompe
  - rapprocher les mains, les pouces et index forment un diamant
  - plier les bras jusqu’à ce que la poitrine touche les mains
  - garder les bras le plus près possible du buste
  - tendre les bras, éloigner le corps du sol
  - recommencer plusieurs fois


** Centre de gravité

Comme une pompe classique, mais

  - les mains sont plus proche du centre de gravité du corps, au niveau du ventre

  - les mains se tournent pour orienter les doigts vers le côté, voire
    légèrement vers l’arrière


** À impulsion

  - pieds à terre
  - mains à terre, au niveau des épaules
  - dos bien aligné
  - plier les bras, laisser le corps se rapprocher du sol
  - tendre les bras, éloigner le corps du sol
  - faire glisser le corps vers l’arrière en pliant les genoux et en
    faisant pivoter les bras vers l’avant par rapport aux épaules
  - reprendre la position initiale
  - recommencer plusieurs fois


** Hindoue

  - former un V inversé, pieds et mains au sol
  - plier les bras pour que la tête se rapproche du sol
  - la tête continue en avançant près du sol
  - rapprocher le bassin du sol
  - cambrer le dos, la tête commence à se relever
  - pousser sur le bras pour accentuer le mouvement de redressement de
    la tête
  - revenir au V inversé de depart
  - recommencer plusieurs fois


** Burpee

  - commencer debout
  - plier les genoux, placer les mains au sol (position accroupie)
  - faire sauter les pieds pour tendre les jambes
    + on se retrouve en position de départ d’une pompe
  - faire une pompe
  - faire sauter les pieds pour plier les jambes et se retrouver à
    nouveau en position accroupie
  - pousser sur les jambes pour se redresser et faire un saut en l’air
  - recommencer plusieurs fois


* Planches

#+TOC: headlines 1 local


** Classique

  - se placer en position initiale de pompe
  - rester ainsi à peu près 30 secondes


** Étendue

  - se placer en position initiale de pompe
  - écarter les bras et les jambes
  - laisser le corps se rapprocher du sol
  - rester ainsi à peu près 30 secondes


** Latérale

  - se placer en position initiale de pompe
  - déplacer bras et jambes vers la droite
  - déplacer bras et jambes vers la gauche
  - recommencer plusieurs fois


** Saut latéral

  - se placer en position initiale de pompe
  - faire sauter les jambes vers la droite
  - faire sauter les jambes vers la gauche
  - recommencer plusieurs fois


** Glissade

  - se placer en position initiale de pompe
  - placer un avant-bras sur le sol
  - placer l’autre avant-bras sur le sol
  - faire glisser le corps vers l’avant
    + en pliant les coudes
    + en dépliant les chevilles
  - faire glisser le corps vers l’arrière
    + en dépliant les coudes
    + en pliant les chevilles
  - recommencer plusieurs fois


** Balançoire

  - se placer en position initiale de pompe
  - placer un avant-bras sur le sol
  - placer l’autre avant-bras sur le sol
  - tourner autour de l’axe du corps vers la gauche
    + buste
    + jambes
    + pieds
  - tourner autour de l’axe du corps vers la droite
  - recommencer plusieurs fois


** Croisée

  - se placer en position initiale de pompe
  - amener le genou gauche jusqu’au coude droit
  - retourner à la position de départ
  - amener le genou droit jusqu’au coude gauche
  - recommencer plusieurs fois


** Sur une jambe

  - se placer en position initiale de pompe
  - placer un avant-bras sur le sol
  - placer l’autre avant-bras sur le sol
  - lever une jambe
  - rester ainsi à peu près 30 secondes
  - reposer la jambe
  - lever l’autre jambe
  - rester ainsi à peu près 30 secondes
  - recommencer plusieurs fois


** Coudes

  - se placer en position initiale de pompe
  - placer un avant-bras sur le sol
  - placer l’autre avant-bras sur le sol
  - replacer une main sur le sol, bras tendu
  - replacer l’autre main sur le sol, bras tendu
  - recommencer plusieurs fois


** Sur un coude

  - se placer en position initiale de pompe
  - placer un avant-bras sur le sol
  - placer l’autre bras le long du corps
  - rester ainsi à peu près 30 secondes
  - recommencer en inversant le rôle des bras


** En poussée

  - se placer en position initiale de pompe
  - placer un avant-bras sur le sol
  - placer l’autre avant-bras sur le sol
  - pousser sur le sol avec les avant-bras et les pieds pendant à peu
    près 30 secondes
  - recommencer plusieurs fois


* Sur le dos

#+TOC: headlines 1 local

** Jambes verticales

  - se placer allongé sur le dos
  - soulever les jambes à la verticale en les gardant tendues
  - laisser retomber doucement les jambes sur le sol


** Jambes sur le ventre

  - se placer allongé sur le dos
  - ramener les jambes sur le ventre
  - laisser retomber doucement les jambes sur le sol


** Abdominaux les bras verticaux

  - lever les bras verticalement
  - redresser le buste verticalement tout en maintenant les bras verticaux
  - revenir en position allongée, les bras restent verticaux
  - répéter


** Moulins                                                              :mma:

alias : Windmill

Petits cercles

  - dos au sol
  - faire mouliner les pieds sur deux cercles qui se chevauchent
    - intérieur vers le bas, extérieur vers le haut
    - intérieur vers le haut, extérieur vers le bas

Grands cercles

  - dos au sol
  - jambes levées sur les côtés
  - faire tourner les jambes une autour de l’autre vers la gauche
	  - jambe gauche dans le prolongement du buste
	  - jambe droite +/- perpendiculaire au buste
	  - le buste suit vers la gauche
  - jambes levées sur les côtés
  - faire tourner les jambes une autour de l’autre vers la droite
	  - jambe droite dans le prolongement du buste
	  - jambe gauche +/- perpendiculaire au buste
	  - le buste suit vers la gauche


** Bicyclette

  - se placer allongé sur le dos
  - lever la tête
  - lever les jambes
  - placer les mains derrière la tête
  - tourner la tête et les mains vers la droite
    + ramener la jambe droite jusqu’au coude gauche
    + la jambe gauche est tendue sans toucher le sol
  - tourner la tête et les mains vers la gauche
    + ramener la jambe gauche jusqu’au coude droit
    + la jambe droite est tendue sans toucher le sol


** Scarabée                                                         :systema:

  - se placer allongé sur le dos / ventre
  - ramener les jambes sur le ventre
  - onduler du dos avancer / reculer


** Serpent                                                          :systema:

  - se placer allongé sur le dos / ventre
  - onduler pour avancer / reculer


** Chenille                                                         :systema:

  - se placer allongé sur le dos / ventre
  - alterner
    + courber puis détendre le dos pour avancer / reculer
    + courber puis détendre les jambes pour avancer / reculer


* Sur le ventre

#+TOC: headlines 1 local


** Extension dorsale

  - se tenir allongé sur le ventre, les bras le long du corps
  - décoller la tête, la poitrine et les jambes du sol
  - rester ainsi quelques secondes
  - laisser retomber doucement


** Criquet                                                          :systema:

  - se tenir allongé sur le ventre
  - placer les poings au sol
  - soulever le bassin d'un coup sec pour soulever légèrement les
    mains et les pieds et avancer


** Salamandre                                                       :systema:

Avancer

  - avancer la main droite
    + placer la main gauche près de la hanche gauche
    + replier la jambe gauche près de la main gauche
    + déplier la jambe droite si nécessaire
  - la main gauche pousse le haut du corps vers la droite
    + le coude gauche se déplie
    + le genou gauche se déplie
    + le coude droit se plie
    + le genou droit se plie près de la main droite
    + le buste et la tête suivent à droite
  - avancer la main gauche
  - la main droite pousse le haut du corps vers la gauche
    + le coude droit se déplie
    + le genou droit se déplie
    + le coude gauche se plie
    + le genou gauche se plie près de la main gauche
    + le buste et la tête suivent à gauche
  - recommencer

reculer

  - reculer la main droite
    + placer la main gauche près de la hanche gauche, mais devant la main droite
    + replier la jambe gauche près de la main gauche
    + déplier la jambe droite si nécessaire
  - la main gauche pousse le haut du corps vers la droite
    + le coude gauche se déplie
    + le genou gauche se déplie
    + le coude droit se plie
    + le genou droit se plie près de la main droite
    + le buste et la tête suivent à droite
  - reculer la main gauche
  - la main droite pousse le haut du corps vers la gauche
    + le coude droit se déplie
    + le genou droit se déplie
    + le coude gauche se plie
    + le genou gauche se plie près de la main gauche
    + le buste et la tête suivent à gauche
  - recommencer


* Genoux

#+TOC: headlines 1 local

** Fente en balançoire

  - debout
  - pieds joints
  - reculer la jambe droite et plier le genou jusqu’à ce que pied &
    genou droits soient au sol
  - se redresser et faire un pas en avant avec la jambe droite
    jusqu’à dépasser la jambe gauche
  - plier le genou gauche jusqu’à ce que pied & genou gauches soient au sol
  - se redresser et faire un pas en arrière avec la jambe droite
  - plier le genou droit jusqu’à ce que pied & genou droits soient au sol
  - recommencer plusieurs fois
  - recommencer en faisant des pas avec la jambe gauche


* Sauts

#+TOC: headlines 1 local

** Ciseaux

  - debout
  - pieds rapprochés
  - sauter et écarter légèrement les jambes
    + lever les bras
  - retomber avec les jambes légèrement écartées et les bras en l’air
  - sauter et rapprocher les jambes
    + descendre les bras
  - retomber avec les jambes rapprochées et les bras le long du corps
  - recommencer plusieurs fois


* Près du sol


** Roulade avant

  - genoux par terre
  - tourner la tête du côté 1
  - rouler vers l’avant autour du bras A_2
    - placer le bras A_2 sur le sol, main entre les genoux
    - la main A_1 est au sol, en soutien


** Roulade arrière

  - assis par terre
  - rouler dos au sol
  - tourner la tête du côté 1
  - rouler vers l’arrière autour du bras A_2
    - placer le bras A_2 sur le sol
    - lancer les jambes vers l’arrière
    - le bras A_1 vient en support près de la tête


** Triangle                                                             :mma:

  - assis par terre
  - rouler dos au sol
  - profiter de l’élan pour soulever le bas du dos et les jambes
    - s’appuyer sur les coudes
  - placer la jambe A_1 sous le genou A_2
  - recommencer en inversant les rÃ´les des jambes


** Jan Jax                                                              :mma:

  - dos au sol
  - la jambe A_1 se replie sous le bassin, pied vers le côté 2
  - redressement
    - sur la jambe et le genou A_1
    - sur le pied A_2
  - dos au sol
  - la jambe A_2 se replie sous le bassin, pied vers le côté 1
  - redressement
    - sur la jambe et le genou A_2
    - sur le pied A_1


** Assis en travers                                                     :mma:

alias : Sit Through

  - dos au sol
  - plier la jambe A_1 pour soulever le genou
  - faire passer la jambe A_2 sous la jambe A_1
  - rouler vers le côté 2 jusqu’à se retrouver sur le ventre
  - se replacer dos au sol
  - plier la jambe A_2 pour soulever le genou
  - faire passer la jambe A_1 sous la jambe A_2
  - rouler vers le côté 1 jusqu’à se retrouver sur le ventre


** Assis Extérieur                                                      :mma:

alias : Sit Out

  - coudes et genoux au sol
  - soulever le coude A_1
  - faire passer la jambe A_2 en diagonale vers le côté 1


** Singe                                                            :systema:

  - se mettre accroupi
  - mains au sol
  - alterner
    + sauts latérals des mains, pieds sur le sol
    + sauts latérals des pieds, mains sur le sol


** Araignée

  - se placer accroupi vers l’arrière
  - avancer
    + avancer le pied gauche
    + amener la main droite jusqu’au pied droit
    + avancer le pied droit
    + amener la main gauche jusqu’au pied gauche
    + répéter
  - reculer
    + reculer la main gauche
    + amener le pied droit jusqu’à la main droite
    + reculer la main droite
    + amener le pied gauche jusqu’à la main gauche
    + répéter


** Panthère

  - se placer à quatre pattes
  - les genoux décollés du sol
  - avancer
    + avancer la main droite
    + amener le genou gauche jusqu’à la main gauche
    + avancer la main gauche
    + amener le genou droit jusqu’à la main droite
    + répéter
  - reculer
    + reculer le genou droit
    + amener la main gauche jusqu’au genou gauche
    + reculer le genou gauche
    + amener la main droite jusqu’au genou droit
    + répéter


* Alternance hauteur & sol


** Tomber

Intérieur

  - debout
  - se laisser descendre accroupi, comme dans un cylindre
  - tourner le genou A_1 vers l’intérieur
  - spiraler vers le sol en tournant vers le côté 2
  - le genou A_1 touche le sol
  - le flanc A_1 touche le sol

Extérieur

  - debout
  - se laisser descendre accroupi, comme dans un cylindre
  - tourner le genou A_1 vers l’extérieur
  - spiraler vers le sol en tournant vers le côté 1
  - le genou A_1 touche le sol
  - le flanc A_1 touche le sol


** Se relever                                                           :mma:

alias : Technical Stand Up

  - dos au sol
  - redressement
    - sur la main A_1 et le pied A_2
    - main a_2 et pied A_1 en l’air
  - faire passer le pied A_1 en arrière, au sol
  - se relever

De l’autre côté

  - dos au sol
  - redressement
	  - sur la main A_2 et le pied A_1
	  - main a_1 et pied A_2 en l’air
  - faire passer le pied A_2 en arrière, au sol
  - se relever


** Étalement                                                            :mma:

alias : Sprawl

Avant

  - plier le genou A_1
  - la jambe A_2 se tend
  - la main A_1 touche terre
  - le genou A_1 touche terre
  - le pied A_2 touche terre à l’avant
  - redressement en utilisant la jambe A_2

Côté

  - s’incliner vers l’avant
  - les mains au sol
  - les jambes font un bond sur le côté choisi
  - les jambes se replacent derrière les mains
  - redressement


* Pieds sur le mur


** Rotations du bassin

  - poser les bras et pieds au sol
  - avancer les bras pour qu’ils forment un angle aigu avec le sol
  - poser les pieds sur le mur
    + pour ne pas glisser, il ne faut pas les placer trop près du sol
  - effectuer des rotations du bassins
    + dans les sens horlogiques et anti-horlogiques
    + dans le plan
      * horizontal
      * vertical, perpendiculaire au mur
      * vertical, parallèle au mur
