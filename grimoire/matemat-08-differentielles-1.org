
#+STARTUP: showall

#+TITLE: Eclats de vers : Matemat 08 : Différentielles - 1
#+AUTHOR: chimay
#+EMAIL: or du val chez gé courriel commercial
#+LANGUAGE: fr
#+LINK_HOME: file:../index.html
#+LINK_UP: file:index.html
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../style/defaut.css" />

#+OPTIONS: H:6
#+OPTIONS: toc:nil

#+TAGS: noexport(n)

[[file:index.org][Index des Grimoires]]

#+INCLUDE: "../include/navigan-1.org"

#+TOC: headlines 1

#+INCLUDE: "../include/commandes-tex.org"

* Différentielles

#+TOC: headlines 1 local

\label{chap:differentielles}


** Dépendances

  - Chapitre \ref{chap:lineaire} : Les fonctions linéaires


** Définition

Soit les espaces vectoriels $\Omega, F$ sur $\corps$.

L'idée à la base de la notion de différentielles est de linéariser localement une fonction $f : \Omega \mapsto F$ autour d'un point $a \in \Omega$. Pour tout $h \in \Omega$ suffisamment petit, on veut donc avoir :

$$f(a + h) - f(a) \approx \differentielle{f}{a}(h)$$

où $\differentielle{f}{a}$ est une application linéaire de $\Omega$ vers $F$. On suppose que la norme existe de $\differentielle{f}{a}$ existe, de sorte que nous puissions écrire :

$$\norme{ \differentielle{f}{a}(h) } \le \norme{ \differentielle{f}{a} } \cdot \norme{h}$$

On voit que la norme de la différentielle tend plus vite que $h$ vers $0$.
On demande que la norme de l'erreur donnée par :

$$E(h) = f(a + h) - f(a) - \differentielle{f}{a}(h)$$

devienne négligeable par rapport à :

$$\norme{ \differentielle{f}{a} } \cdot \norme{h}$$

lorsque $h$ tend vers $0$. Comme la norme de la différentielle ne varie pas, il nous suffit d'imposer que :

$$\lim_{ \substack{ h \to 0 \\ h \ne 0 } } \frac{ \norme{E(h)} }{ \norme{h} } = 0$$

Si ces conditions sont vérifiées, on dit que $f$ est différentiable en $a$ et que $\differentielle{f}{a}$ est la différentielle de $f$ en $a$. On a alors :

$$f(a + h) - f(a) = \differentielle{f}{a}(h) + E(h)$$


*** Notation

Tout au long de ce chapitre, nous notons :

#+BEGIN_CENTER
\(
\lim_{h \to 0} = \lim_{ \substack{ h \to 0 \\ h \ne 0 } } \\
\lim_{b \to a} = \lim_{ \substack{ b \to a \\ b \ne a } }
\)
#+END_CENTER


** Continuité

Si $a$ est différentiable en $a$, elle est forcément continue en $a$. En effet, la différentielle est bien continue :

$$\norme{ \differentielle{f}{a}(0) } \le \norme{ \differentielle{f}{a} } \cdot \norme{0} = 0$$

Par définition, pour tout $\epsilon \strictsuperieur 0$, on peut trouver $\delta \strictsuperieur 0$ tel que :

$$\frac{ \norme{E(h)} }{ \norme{h} } \le \epsilon$$

c'est-à-dire :

$$\norme{E(h)} \le \epsilon \cdot \norme{h}$$

On en déduit que l'erreur est continue en $h = 0$. On a donc :

$$\lim_{h \to 0} \norme{E(h)} = 0$$

L'expression de $f(a + h)$ peut donc s'écrire :

$$\lim_{h \to 0} f(a + h) = f(a) + \lim_{h \to 0} \left[ \differentielle{f}{a}(h) + E(h) \right] = f(a)$$


** Dérivées partielles

Nous allons à présent voir comment obtenir les composantes de la différentielle dans le cas d'espaces de dimensions finies. Nous disposons donc d'une base $(\varpi_1,...,\varpi_n)$ de $\Omega$ et d'une base $(\phi_1,...,\phi_m)$ de $F$. Nous introduisons les composantes $f_1,...,f_m : \Omega \mapsto F$ de $f$ telles que :

$$f(x) = \sum_{i = 1}^m f_i(x) \cdot \phi_i$$

pour tout $x \in \Omega$. Nous procédons de même pour l'erreur :

$$E(h) = \sum_{i = 1}^m E_i(h) \cdot \phi_i$$

pour tout $h \in \Omega$. Nous allons également utiliser les coordonnées $h_1,...,h_n \in \corps$ de $h$ :

$$h = \sum_{i = 1}^n h_i \cdot \varpi_i$$

Par linéarité :

$$f(a + h) - f(a) = \sum_{j = 1}^n \differentielle{f}{a}(\varpi_j) \cdot h_j + E(h)$$

Mais on peut trouver des $\Delta_{ij} \in \corps$ tels que :

$$\differentielle{f}{a}(\varpi_j) = \sum_{i = 1}^m \Delta_{ij} \cdot \phi_i$$

Injectons les expressions des composantes dans $F$. On obtient :

$$\sum_i \phi_i \cdot \left[ f_i(a + h) - f_i(a) - E_i(h) - \sum_{j = 1}^n \Delta_{ij} \cdot h_j \right] = 0$$

Par indépendance linéaire des $\phi_i$, on a alors :

$$f_i(a + h) - f_i(a) = \sum_{j = 1}^n \Delta_{ij} \cdot h_j + E_i(h)$$

pour tout $i \in \setZ(1,m)$. On nomme $\Delta_{ij}$ la dérivée partielle de $f_i$ par rapport à $\varpi_j$. On la note :

$$\partial_j f_i(a) = \deriveepartielle{f_i}{x_j}(a) = \Delta_{ij}$$


*** Attention

Ne pas confondre la frontière $\frontiere A$ d'un ensemble $A$ avec la dérivée $\partial f$ d'une fonction $f$.


** Dérivées et limites

Soit $\lambda \in \corps$. Si l'on choisit $h = \lambda \ \varpi_j$, on a $h_k = \lambda \ \indicatrice_{jk}$ et :

\begin{align}
f_i(a + \lambda \ \varpi_j) - f_i(a) &= \sum_{k = 1}^n \partial_k f_i(a) \cdot \lambda \cdot \indicatrice_{jk} +  E_i(h) \\
&= \lambda \cdot \partial_j f_i(a)  +  E_i(h)
\end{align}

En divisant l'équation ci-dessus par $\lambda$ puis en faisant tendre $\lambda$ vers 0, on obtient :

$$\partial_j f_i(a) = \lim_{\lambda \to 0} \left[ \frac{f_i(a + \lambda \ \varpi_j) - f_i(a)}{\lambda} - \frac{E_i(h)}{\lambda} \right]$$

Comme l'erreur doit converger plus vite vers zéro que la norme $\norme{h} = \lambda$, la limite du second terme du membre de droite s'annule et on a :

$$\partial_j f_i(a) = \lim_{\lambda \to 0} \frac{f_i(a + \lambda \ \varpi_j) - f_i(a)}{\lambda}$$

Ce qui montre que la dérivée partielle $\partial_j f_i$ est la variation de $f_i$ obtenue lorsqu'on fait varier la $j^{ème}$ variable (celle correspondant à $\varpi_j$).


** Représentation matricielle

On associe à la différentielle $\differentielle{f}{a}$ la matrice Jacobienne $\partial f(a)$ de $f$ en $a$ définie par :

$$\partial f(a) = \Big( \partial_j f_i(a) \Big)_{i,j}$$

On peut alors écrire la linéarisation de $f$ sous forme de produit matriciel :

$$f(a + h) - f(a) = \partial f(a) \cdot h + E(h)$$

où $f,h,E$ sont les vecteurs colonnes associés aux grandeurs du même nom.


*** Vecteurs associés

On dispose de $n$ vecteurs représentant chacun la dérivée des composantes de $f$ par rapport à la $j^{ème}$ variable :

$$\partial_j f(a) = \Big( \partial_j f_i(a) \Big)_i$$

et de $m$ vecteurs représentant chacun les dérivées de la $i^{ème}$ composante de $f$ :

$$\partial f_i(a) = \Big( \partial_j f_i(a) \Big)_j$$


*** Notation

Dans le cas où la variable porte un nom par défaut, comme par exemple :

$$f : x \mapsto f(x)$$

on note aussi :

$$\deriveepartielle{f}{x}(a) = \partial f(a)$$


**** Plusieurs sous-variables

Lorsque plusieurs sous variables portent un nom par défaut, comme par exemple :

$$f : (x,y) \mapsto f(x,y)$$

on note aussi :

$$\deriveepartielle{f}{x}(a) = \partial_x f(a)$$

pour la Jacobienne par rapport aux variables $x = (x_1,...,x_s)$ et :

$$\deriveepartielle{f}{y}(a) = \partial_y f(a)$$

pour la Jacobienne par rapport aux variables $y = (y_1,...,y_t)$


**** Symbolique

On a aussi les notations symboliques :

#+BEGIN_CENTER
\(
df = \deriveepartielle{f}{x^T} \ dx \\
df_i = \sum_j \deriveepartielle{f_i}{x_j} \ dx_j
\)
#+END_CENTER

où $df$ représente une petite variation de $f$ suite à une petite variation $dx$ de $x$.

On utilise parfois la transposée de la Jacobienne :

$$\Big[ \partial f(x) \Big]^T = \deriveepartielle{f^T}{x} = \left( \deriveepartielle{f}{x^T} \right)^T$$


*** Appellation

Pour des fonctions du type $f : \setR^n \mapsto \setR$, la Jacobienne se réduit à un vecteur matriciel. On dit alors que $\partial f$ est le gradient de $f$.


** Dérivées ordinaires

Dans le cas où $m = n = 1$, il n'y a qu'une dérivée partielle, $\partial_1 f_1$, que l'on appelle alors dérivée ordinaire. On a les équivalences :

$$\lambda \ \phi_1 \quad \Leftrightarrow \quad \lambda \quad \Leftrightarrow \quad \lambda \ \epsilon_1$$

On peut considérer $\Omega$ et $F$ comme équivalents à $\corps$ et se restreindre à des fonctions $f : \corps \mapsto \corps$ sans perte de généralité. La dérivée ordinaire est alors simplement :

$$\partial f(a) = \lim_{\lambda \to 0} \frac{f(a + \lambda) - f(a)}{\lambda}$$


*** Notation

On note aussi :

$$\OD{f}{x}(a) = \lim_{\lambda \to 0} \frac{f(a + \lambda) - f(a)}{\lambda}$$

On a alors :

$$f(a + h) = f(a) + \OD{f}{x}(a) \cdot h + E(h)$$

et :

$$\lim_{\lambda \to 0} \frac{E(\lambda)}{\lambda} = \lim_{\lambda \to 0} \left[ \frac{f(a + \lambda) - f(a)}{\lambda} - \OD{f}{x}(a) \right] = 0$$


*** Définition équivalente

Si on pose $b = a + \lambda$, on voit que $\lambda = b - a$ et que la convergence $h \to 0$ est équivalente à $b \to a$. On a donc :

$$\OD{f}{x}(a) = \lim_{b \to a} \frac{f(b) - f(a)}{b - a}$$


** L'application dérivée

Si $f$ est différentiable en tout vecteur $a$ de $A \subseteq \Omega$, on dit que $f$ est différentiable sur $A$. On peut alors définir une application dérivée $\partial f : A \mapsto \matrice(\corps,m,n)$ définie par :

$$\partial f : a \mapsto \partial f(a)$$

pour tout $a \in A$. Si cette nouvelle application $\partial f$ est également continue, on dit que $f$ est continûment différentiable. On note $\continue^1(A,F)$ l'ensemble des fonctions continûment différentiables de $A$ vers $F$.


** Hessienne

Soit $f : \Omega \mapsto \corps$ avec $\Omega$ de dimension finie $n$. Supposons que $f$ est différentiable sur $A \subseteq \Omega$. La dimension de $\corps$ sur $\corps$ étant $1$, la Jacobienne $\partial f(a)$ se réduit à un vecteur matriciel de composantes $\partial_i f$. Si la dérivée $\partial f : A \mapsto \corps^n$ est elle-même différentiable, on nomme l'application définie par :

$$\partial^2 f = \partial \left( \partial f \right)$$

la dérivée seconde de $f$. Il s'agit d'une fonction qui transforme un élément de $A$ en un « vecteur matriciel de vecteurs matriciels » appartenant à $\matrice(\corps^n,n,1)$. On peut assimiler cet objet à une matrice équivalente de taille $(n,n)$ dont les composantes sont des éléments de $\corps$. En définitive, nous avons $\partial^2 f(a) \in \matrice(\corps,n,n)$ pour tout $a \in A$. Cette matrice est appellée hessienne de $f$ en $a$. Ses composantes sont données par :

$$\partial_{ij}^2 f(a) = \partial_i \left( \partial_j f \right)(a)$$


*** Notation

On note aussi :

$$\dfdxdy{f}{x_i}{x_j}(a) = \partial_{ij}^2 f(a)$$

Lorsque $i = j$, on note :

$$\dfdxdx{f}{x_i} = \dfdxdy{f}{x_i}{x_i} = \partial_{ii}^2 f(a)$$

En termes matriciels, cela donne :

$$\dblederiveepartielle{f}{x}(a) = \partial^2 f(a)$$


*** Dérivée ordinaire

Dans le cas où la dimension de $\Omega$ est un $1$, on peut l'assimiler à $\corps$, on a alors une fonction $f : \corps \mapsto \corps$ possédant une seule dérivée seconde, que l'on note :

$$\OOD{f}{t}(a) = \partial^2 f(a)$$


*** Continuité

On note $\continue^2(A,\corps)$ l'ensemble des fonctions dont la dérivée seconde est continue sur $A$.


** Dérivée d'ordre $k$

Soit $f : \corps \mapsto \corps$ et $k \in \setN$. Pour autant que la fonction $f$ soit suffisamment dérivable, on définit par récurrence la fonction $\partial^k f : \corps \mapsto \corps$ :

\begin{align}
\partial^0 f &= f \\
\partial^k f &= \partial \big( \partial^{k-1} f \big)
\end{align}

La dérivée d'ordre $k$  de $f$ est donc la fonction obtenue lorsqu'on applique un nombre $k$ de fois l'opérateur de dérivation $\partial$ à la fonction $f$ :

$$\partial^k f = (\partial \circ ... \circ \partial)(f)$$


*** Ensembles

On note $\continue^k(A,\corps)$ l'ensemble des fonctions $f : A \mapsto \corps$ dont la dérivée d'ordre $k$ :

$$\partial^k f : A \mapsto \corps$$

existe et est continue sur $A$.


**** Infini

Si la dérivée $\partial^k f$ existe pour tout $k \in \setN$, on dit que $f$ est indéfiniment dérivable. On note $\continue^\infty(A,\corps)$ l'ensemble des fonctions indéfiniment dérivables.


**** Ordre $0$

On voit que $\continue^0 = \continue$.


*** Homéomorphisme

On note $\homeomorphisme^k(A,\corps)$ l'ensemble des bijections $f$ de $\continue^k$ telles que la fonction $f^{-1}$ soit aussi dans $\continue^k$.


*** Notations

Pour $\alpha = (\alpha_1, \alpha_2, ..., \alpha_n)$, on écrit également :

#+BEGIN_CENTER
\(
\partial^\alpha f = \partial^{\alpha_1 ... \alpha_n} f =
\partial_1^{\alpha_1} ... \partial_n^{\alpha_n} f
\)
#+END_CENTER


** Fonctions à intégrale continue

Soit une fonction $u : A \mapsto B$. Si la fonction $v$ définie par :

$$v(x) = \int_a^x u(x) d\mu(x)$$

est continue, on dit que $u$ est à intégrale continue. On note $\continue_{\mu}^{-1}(A,B)$ l'ensemble des fontions à intégrale continue.


** Différentiabilité uniforme

On dit qu'une fonction $f$ est uniformément différentiable sur $A$ si pour tout $\epsilon \strictsuperieur 0$, on peut trouver un $\delta \strictsuperieur 0$ tel que :

$$\abs{f(s) - f(t) - \partial f(t) \cdot (s - t)} \le \epsilon \cdot \abs{s - t}$$

quel que soit $s,t \in A$ vérifiant $\abs{s - t} \le \delta$.


* Dérivées

#+TOC: headlines 1 local

\label{chap:derivee}


** Dépendances

  - Chapitre \ref{chap:differ} : Les différentielles


** Fonction constante

Si $f$ est constante, on peut trouver $c \in F$ tel que :

$$f(x) = c$$

pour tout $x \in \Omega$. On a alors :

$$\partial_j f(a) = \lim_{\lambda \to 0} \frac{c - c}{\lambda} = 0$$

Par conséquent :

$$\partial f(a) = 0$$


** Identité

Considérons le cas particulier où $m = n$ et où $f = \identite$. Lorsque $i = j$, nous avons :

$$\partial_i f_i(a) = \lim_{\lambda \to 0} \frac{x_i + \lambda - x_i}{\lambda} = \lim_{\lambda \to 0} \frac{\lambda}{\lambda} = 1$$

Lorsque $i \ne j$, on a par contre :

$$\partial_j f_i(a) = \lim_{\lambda \to 0} \frac{x_i - x_i}{\lambda} = \lim_{\lambda \to 0} \frac{0}{\lambda} = 0$$

On en conclut que :

$$\partial f(a) = \partial \identite(a) = ( \indicatrice_{ij} )_{i,j} = I$$

La Jacobienne de la fonction identité est la matrice identité.


** Composition de fonctions

Nous allons nous intéresser à présent au moyen d'obtenir la dérivée d'une composée de fonctions $f : E \mapsto F$ et $g : F \mapsto G$. Supposons que $f$ soit différentiable en $a$ et que $g$ soit différentiable en $b = f(a)$. On a alors :

#+BEGIN_CENTER
\(
df(a) = f(a + da) - f(a) = \partial f(a) \cdot da + E_f(da) \\
dg(b) = g(b + db) - g(b) = \partial g(b) \cdot db + E_g(db)
\)
#+END_CENTER

Choisissons en particulier $db = f(a + da) - f(a)$. On a alors :

\begin{align}
dg(b) &= g(f(a + da)) - g(f(a)) \\
&= (g \circ f)(a + da) - (g \circ f)(a) \\
&= d(g \circ f)(a)
\end{align}

Mais d'un autre coté :

\begin{align}
dg(b) &= \partial g(f(a)) \cdot df(a) + E_g(df(a)) \\
&= \partial g(f(a)) \cdot \left( \partial f(a) \cdot da + E_f(da) \right) + E_g(df(a)) \\
&= \partial g(f(a)) \cdot \partial f(a) \cdot da + E_f(da) \cdot da + E_g(df(a)) \\
\end{align}

Il est aisé de vérifier que :

$$\lim_{h \to 0} \frac{\partial g(f(a)) \cdot E_f(da)  + E_g(df(a))}{\norme{h}} = 0$$

puisque $\partial g(f(a))$ ne dépend pas de $da$. On a donc montré que $g \circ f$ est différentiable en $a$ et que :

$$\partial (g \circ f)(a) =  \partial g(f(a)) \cdot \partial f(a) = (\partial g \circ f)(a) \cdot \partial f(a)$$

La dérivée d'une composée de fonctions est donc tout simplement le produit des Jacobiennes.


*** Notation

Soit le schéma fonctionnel :

$$(x_1,...,x_n) \mapsto (y_1,...,y_m) \mapsto (z_1,...,z_p)$$

On note aussi :

#+BEGIN_CENTER
\(
\deriveepartielle{z_i}{x_j} = \sum_k \deriveepartielle{z_i}{y_k} \cdot \deriveepartielle{y_k}{x_j} \\ \\
\deriveepartielle{z}{x^T} = \deriveepartielle{z}{y^T} \cdot \deriveepartielle{y}{x^T}
\)
#+END_CENTER

Pour $z : t \mapsto (x_1(t),x_2(t),...,x_n(t))$, on a également :

$$\OD{z}{t} = \sum_k \deriveepartielle{z}{x_k} \cdot \OD{x_k}{t}$$

Si $x,y,z \in \corps$, on a encore :

$$\OD{z}{x} = \OD{z}{y} \cdot \OD{y}{x}$$


** Inverse fonctionnel

En considérant le cas particulier $g = f^{-1}$, on a $g \circ f = \identite$. Choisissons un vecteur $a$ où $f$ est différentiable et posons $b = f(a)$. On a :

$$\partial f^{-1}(b) \cdot \partial f(a) = \partial \identite(a) = I$$

La jacobienne de l'inverse d'une fonction est donc l'inverse matriciel de sa jacobienne :

$$\partial f^{-1}(b) = \left[ \partial f(a) \right]^{-1}$$


*** Notation

On note aussi :

$$\deriveepartielle{y}{x}  = \left(\deriveepartielle{x}{y}\right)^{-1}$$

ou, pour des fonctions $y : \corps \mapsto \corps$ :

$$\OD{y}{x}  = \left(\OD{x}{y}\right)^{-1}$$


** Addition

Si $f,g$ sont différentiables en $a$, on a :

#+BEGIN_CENTER
\(
f(a + h) - f(a) = \partial f(a) \cdot h + E_f(h) \\
g(a + h) - g(a) = \partial g(a) \cdot h + E_g(h)
\)
#+END_CENTER

En additionnant les équations ci-dessus, on obtient :

#+BEGIN_CENTER
\(
\left[ f(a + h) + g(a + h) \right] - \left[ f(a) + g(a) \right]
= [ \partial f(a) + \partial g(a) ] \cdot h \\
\qquad \qquad + E_f(h) + E_g(h)
\)
#+END_CENTER

Il est clair que :

$$\lim_{h \to 0} \frac{\norme{E_f(h) + E_g(h)}}{\norme{h}} = 0$$

La fonction $f + g$ est donc différentiable en $a$ et :

$$\partial (f + g)(a) = \partial f(a) + \partial g(a)$$

On peut montrer, cette fois en soustrayant les deux équations que :

$$\partial (f - g)(a) = \partial f(a) - \partial g(a)$$


** Produit scalaire

Considérons deux fonctions $f,g$ différentiables en $a$ :

#+BEGIN_CENTER
\(
f(a + h) = f(a) + \partial f(a) \cdot h + E_f(h) \\
g(a + h) = g(a) + \partial g(a) \cdot h + E_g(h) \\
\)
#+END_CENTER

Leur produit scalaire s'écrit :

#+BEGIN_CENTER
\(
\scalaire{f(a + h)}{g(a + h)} = \scalaire{f(a)}{g(a)} + \scalaire{f(a)}{\partial g(a) \cdot h} + \scalaire{\partial f(a) \cdot h}{g(a)} \\
\qquad \qquad \qquad + E_{f \cdot g}(h)
\)
#+END_CENTER

où :

#+BEGIN_CENTER
\(
E_{f \cdot g}(h) = \scalaire{E_f(h)}{E_g(h)} + \scalaire{E_f(h)}{g(a)} + \scalaire{E_f(h)}{\partial g(a)} + \\
\qquad \qquad \scalaire{f(a)}{E_g(h)} + \scalaire{\partial f(a)}{E_g(h)}
\)
#+END_CENTER

On a donc :

$$\lim_{h \to 0} \frac{\norme{E_{f \cdot g}(h)}}{\norme{h}} = 0$$

ce qui nous montre que la différentielle du produit scalaire s'écrit :

$$\differentielle{ \scalaire{f}{g} }{a}(h) = \scalaire{f(a)}{\partial g(a) \cdot h} + \scalaire{\partial f(a) \cdot h}{g(a)}$$

En terme de composantes, on a :

$$\differentielle{ \scalaire{f}{g} }{a}(h) = \sum_{j = 1}^n \varpi_j \cdot h_j \cdot \sum_{i = 1}^m \left[ f_i(a) \cdot \partial_j g_i(a) + \partial_j f_i(a) \cdot g_i(a) \right]$$

La représentation matricielle s'écrit donc :

$$\partial \scalaire{f}{g}(a) = \left[ \partial g(a) \right]^T \cdot f(a) + \left[ \partial f(a) \right]^T \cdot g(a)$$


*** Dérivée ordinaire

Dans le cas où $m = n = 1$, cette expression se simplifie en :

$$\OD{}{x}(f \cdot g)(a) = f(a) \cdot \OD{g}{x}(a) +  \OD{f}{x}(a) \cdot g(a)$$


*** Constante

Si une des deux fonctions est constante, soit $g(x) = c$ pour tout $x$, on a :

$$\partial g(x) = 0$$

et :

$$\OD{}{x}(f \cdot c)(a) = f(a) \cdot 0 +  \OD{f}{x}(a) \cdot c = c \cdot \OD{f}{x}(a)$$


*** Notation

On note aussi :

$$d(f \cdot g) = df \cdot g + f \cdot dg$$


** Inverse multiplicatif

Soit les fonctions $f,g : \corps \mapsto \corps$ reliées par l'équation :

$$f \cdot g = 1$$

En dérivant, on obtient :

$$\OD{f}{x} \cdot g + f \cdot \OD{g}{x} = \OD{1}{x} = 0$$

Donc, si $g \ne 0$, on a :

$$\OD{f}{x} = - \frac{f}{g} \cdot \OD{g}{x}$$

Mais comme $f(x) = 1/g(x)$, cela nous donne :

$$\OD{}{x}\left(\unsur{g}\right)(x) = - \unsur{g(x)^2} \cdot \OD{g}{x}(x)$$


** Fraction

En appliquant les résultats précédents, on obtient :

$$\OD{}{x}\left( \frac{f}{g} \right)(x) = \OD{}{x}\left( f \cdot \unsur{g} \right)(x) = \OD{f}{x}(x) \cdot g(x) - \frac{f(x)}{g(x)^2} \cdot \OD{g}{x}(x)$$

et finalement :

$$\OD{}{x}\left( \frac{f}{g} \right)(x) = \frac{\OD{f}{x}(x) \cdot g(x) - f(x) \cdot \OD{g}{x}}{g(x)^2}$$


** Dérivée d'une limite

Soit la suite de fonctions :

$$F = \{ f_n \in \setR^\setR : n \in \setN \}$$

convergeant en tout point $x \in \setR$ vers une fonction $f : \setR \mapsto \setR$ :

$$\lim_{n \to \infty} f_n(x) = f(x)$$

Si la fonction $f$ est différentiable en $a \in \setR$, on a :

\begin{align}
\partial f(a) &= \lim_{h \to 0} \unsur{h} \big[f(a + h) - f(a)\big] \\
&= \lim_{h \to 0} \unsur{h} \crochets{\lim_{n \to \infty} f_n(a + h) - \lim_{n \to \infty} f_n(a)} \\
&= \lim_{h \to 0} \lim_{n \to \infty} \frac{f_n(a + h) - f_n(a)}{h}
\end{align}
