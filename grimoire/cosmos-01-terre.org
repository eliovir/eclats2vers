
#+STARTUP: showall

#+TITLE: Eclats de vers : Cosmos 01 : Terre
#+AUTHOR: chimay
#+EMAIL: or du val chez gé courriel commercial
#+LANGUAGE: fr
#+LINK_HOME: file:../index.html
#+LINK_UP: file:index.html
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../style/defaut.css" />

#+OPTIONS: H:6
#+OPTIONS: toc:nil

#+TAGS: noexport(n)

[[file:index.org][Index des Grimoires]]

#+INCLUDE: "../include/navigan-1.org"

#+TOC: headlines 1

* Orbite autour du soleil


** Année

  - sidérale : le soleil revient au même endroit dans les
    constellations du zodiaque

    + 365 j 6 h 9 m 10 s

  - tropicale : entre deux équinoxes de printemps

    + 365 j 5 h 48 m 46 s


** Cycles

  - précession de l’axe de rotation de la terre
    + 25 920 ans
  - variations de l’inclinaison de l’axe de rotation de la terre
    + 41 000
    + lié aux âges glaciaires ?
    + dernière inclinaison maximale : 8 700 BC
  - rotation de l’apogée (et du périgée) de l’orbite
    + 112 000 ans
    + effet sur le contraste des saisons
  - solstice d’une apogée à une autre
    + dû à la précession et à la rotation de l’apogée
    + 21 000 ans
  - variations de l’excentricité de l’orbite
    + plus ou moins ovale
    + 100 000 ans
    + effet sur le contraste des saisons
  - variations de l’inclinaison de l’orbite par rapport à l’orbite actuelle
    + 70 000 ans
  - variations de l’inclinaison de l’orbite par rapport au plan moyen
    du système solaire (plan invariable)
    + 100 000 ans
    + lié aux âges glaciaires ?

Excellente vidéo sur le sujet (anglais) : [[https://www.youtube.com/watch?v=T4LhlMTNEIM][Earth motion around the sun]].
