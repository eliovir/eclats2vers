
#+STARTUP: showall

#+TITLE: Eclats de vers : Litera : Éclaircies
#+AUTHOR: chimay
#+EMAIL: or du val chez gé courriel commercial
#+LANGUAGE: fr
#+LINK_HOME: file:../index.html
#+LINK_UP: file:index.html
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="../style/defaut.css" />

#+OPTIONS: H:6
#+OPTIONS: toc:nil

#+TAGS: noexport(n)

[[file:index.org][Index littéraire]]

#+INCLUDE: "../include/navigan-1.org"

#+TOC: headlines 1

* Mon vieux stylo rouillé

#+BEGIN_CENTER
#+BEGIN_VERSE
Mon vieux stylo rouillé grince ; il n’est pas content.
Serait-ce le réveil, le temps, les rhumatismes ?
Du tout, me souffle-t-il, sans le moindre mélisme,
Mon auteur m’a laissé de coté trop longtemps.

Tu me savais pourtant allergique aux poussières
Et grand consommateur de ces coupes d’air frais
Qu’on ne trouve qu’à l’ombre immense et centenaire
Des cascades perchées au fin fond des forêts

Tu me savais pourtant argentin de facture
Amateur de tango et de bandonéons
Ce duel ancestral qui mêle en sa morsure
Le venin de la guêpe et le dard du frelon

Mais tu me laisses là, dans ce placard ingrat
Dans cette obscurité juste digne d’un rat
Où j’entends bavarder les livres et les ombres,
Et tout ce bric à brac qui ronfle et qui m’encombre.

- Le temps s’est égaré que je voulais écrire.
Mon essor, à présent, aussi lourd que l’acier
S’est posé sur le roc aride d’un glacier
Où l’air est trop précieux pour oser un sourire.

- Mais c’est de l’encre-en-ciel qui coule dans ton âme
Tu dois ouvrir les yeux pour que les larmes sèchent !
Ces vieux accords brisés n’attendent qu’une flamme
Un souffle, une étincelle, que dis-je une flammèche !

Ami, ouvre les yeux, ce stylo est fort sage :
Rien ne sert d’isoler ton coeur et ta conscience.
Si tout autour de toi te semble de passage
L’esprit est toujours là qui te guide en silence.
#+END_VERSE
#+END_CENTER

* Lueur

#+BEGIN_CENTER
#+BEGIN_VERSE
    Comme une fleur qui pousse au milieu du bitume,
    Comme un îlot défiant les vagues en furie,
    Comme un trait de soleil qui transperce la brume
    Et vient se consumer sur la grève alanguie,

    Comme un pinson éclos d’une orgie de vautour,
    L'éclatante colombe éclipsant les corbeaux,
    Comme un sanglot craché d’un oeil sec, sans amour,
    Pierrerie dans la boue que pleurent les roseaux,

    Comme un cristal de quartz attendant dans sa nuit
    Qu’un rayon de soleil s’en vienne l'épouser,
    Une source fumante au coeur enseveli
    Sous le blizzard épais des sabliers figés,

    La dernière bougie d’un vieux manoir lugubre,
    La rosée matinale au coeur du sahara,
    Un sourire égayant un vieux lit insalubre,
    Un éclair sidéral dans l’encre d’un sous-bois,

    L'Espoir toujours luira, fragile et invincible,
    Absorbant sans effort les ténèbres horribles.
#+END_VERSE
#+END_CENTER

* Aragonienne

#+BEGIN_CENTER
#+BEGIN_VERSE
    Vite courons aux champs fleuris
    Avant que les fleurs ne s’y fanent !
    Que leur reste-t-il dans les bras
    Quand l’oisillon quitte le nid ?

    Nous veillerons les vagues vive-
    Hantées d’un somme ensorcelé
    Nous troublerons d’une onde vive
    Leur peau limpide échevelée

    Sous leurs paupières de géode
    J’y ai revu tes yeux pers sang
    Tapis volants sur pilotis
    Qui entrouvraient leurs antipodes

    Que pouvaient-elles bien cacher
    Dans l’antre obscur du flot amer ?
    Serait-ce un charme que contient
    Ton fier tétin de sauvagine ?

    A la surface de l'étang
    Nous lui élèverons un temple
    De quelques jets d’eau et de pierre
    Où ricocheront nos reflets

    Redevenant souffleurs de vers
    Pieds dans la boue vers l’horizon
    Se ventilant d’un magazine
    Pour mieux sentir venir le vent

    Nous laisserons filer l’oseille
    Dans une attraction sans cardan
    Couchés auprès d’un vieux cadran
    Sur les galets bleus de soleil

    Nous n’aurons plus de lest au mât
    Mais du vélin comme voilure
    Et vorace est son estomac
    Lorsqu’il dévore l’insoluble

    Pour mieux sacrer les sacrilèges
    Entre l’ombre et le crépuscule
    Nous taillerons dans nos malaises
    D’une serpe en croissant de lune

    De ce remède empoisonné
    Nous suerons quelques oasis
    Sous les roseaux déracinés
    L’apnée nous sera bienfaitrice

    Sais-tu que la brume transpire
    Malgré ce froid qui nous canarde ?
    Mais tous les vieux complots transpirent
    Dans l’intempérance pinarde

    Oui ton coeur dort dans ton regard
    Tout voile serait translucide
    Comme un avant-goût de l’extase
    C’est l'âme qui me sert de guide
#+END_VERSE
#+END_CENTER

* Métamorphose

#+BEGIN_CENTER
#+BEGIN_VERSE
    Avec son front squelettique
    Sa frondaison dégarnie
    Privé de toute tunique
    Notre arbre songe aux Antilles

    Il ne se paie pas de mine
    Ainsi pâle et dévêtu
    Les faux bourdons qui butinent
    Ont de longtemps disparu

    Seul un hibou de lucarne
    S’ose poser sur sa branche
    L’hiver rapace s’acharne
    L'écorce claque des hanches

    Mais fol qui l’irait jauger
    A son allure hivernale
    Moi qui l’ai vu argenté
    Tout pomponné de pétales

    Moi qui l’ai pu contempler
    Lorsqu’il était un joyau
    Jouet de ce vent léger
    Qui lui caressait la peau

    Moi qui ai vu sur son tronc
    Ce tendre coeur étranger
    Ce tatouage marron
    Déformé par les années

    Travaillé par la croissance
    Et de malicieuses fées
    Moi qui l’ai lu en silence
    Oui je peux bien l’affirmer

    A l’heure où de doux flocons
    Plus printaniers le couvraient
    Tout n'était qu’ouate et coton
    La neige alors préférait

    Sa charpente aux champs brumeux
    Le duvet au frigidaire
    Les nids chantonnaient joyeux
    Mais le printemps est devers

    L’arbre est tout nu grelottant
    La neige est déchue des cimes
    Et c’est la terre à présent
    Qui se recouvre d’hermine

    Ce qui n'était autrefois
    Qu’un terrain vaguement laid
    Mais nettement improbable
    Devient pur comme le lait

    Que l’on tête à la naissance
    Sur le tétin tout mouillé
    De la douillette espérance
    Ce lait non encor souillé

    Par les basses-cours d’enfance
    Les adultes porcheries
    Les bassesses la souffrance
    Ne l’ont pas encor aigri

    Mais le temps change si vite
    L’azur se zèbre d'éclairs
    Et les arcs-en-ciel invitent
    Quelques ondées passagères

    Les carnets de bal débordent
    Comme une crue de fin mars
    Le déluge les aborde
    La giboulée part en chasse

    Le rythme effréné succède
    Aux languissantes glissades
    A peu que la salsa cède
    Au jazz qui la sérénade

    Puis c’est la ronde enivrante
    De quelque valse à mil temps
    Oui la nature est changeante
    Allant du roide au clément

    Il arrive que les cieux
    Inversent le cours des fleuves
    Que la fumée soit sans feu
    Et que les éclaircies pleuvent
#+END_VERSE
#+END_CENTER

* Le printemps

#+BEGIN_CENTER
#+BEGIN_VERSE
    C’est lui qui te donne des ailes
    Lui qui te rend léger et fou
    Lui qui fait fondre la flanelle
    Le printemps d’acajou

    Quand nous arrivent les chaleurs
    Les oiseaux fêtent son retour
    Le soleil sème les couleurs
    Dessine les contours

    On voit refleurir les bruyères
    En lisière des cerisiers
    Dans la litière des bergères
    S’en viennent les bergers

    Dynamitées de suggestions
    Et habillées de courants d’air
    Les filles au premier frisson
    Entrouvrent leurs clairières

    Laissez-vous choir feuilles de vigne
    Alourdies d’imagination
    Que la pesanteur vous résigne
    Si ce n’est la saison

    Cueillons les corolles ardentes
    Dissimulées sous les buissons
    Déjà les soupirs s’impatientent
    N’entendent plus raison

    C’est le carnaval des oeillets
    Sous les bouquets de confettis
    C’est le carillon du muguet
    Qui danse dans son lit

    Au placard les vieilles pantoufles
    La brise est douce et sensuelle
    La joie sifflote au moindre souffle
    La vie se renouvelle

    Si les averses de pétales
    Rendent un son mélancolique
    Quand le vent du soir qui détale
    Remue comme un moustique

    C’est que le tapis parfumé
    Connaît la musique des arbres
    L'élégie des hivers gravés
    Dans l'écorce de marbre
#+END_VERSE
#+END_CENTER

* Hibernation

#+BEGIN_CENTER
#+BEGIN_VERSE
    Lecteur, suspends ton vol étourdissant d’abeille,
    Laisse filtrer ce conte au creux de ton oreille :
    C’est l’histoire d’un fou flirtant avec l’abîme
    Qui prit un beau matin la douce main des rimes ;

    Un fou jeune et bouillant aux fougues sans pareilles
    Qui flottait libre enfin entre songe et réveil !
    Dans l’encre délivrée les strophes s’engouffrèrent,
    Le délire appelant son suivant comme un frère.

    La révolte grondait dans son âme enflammée,
    Entre les sonnets doux et les fleurs parfumées,
    Rêvant un paradis où les frelons butinent,
    L'épée au ratelier, des bouquets d'églantine.

    Le vent les lui confia, la voix des sortilèges
    Et le secret des chats qui lui firent cortège ;
    Ils furent dispersés, les nuages de toile,
    Invoqués le soleil, la lune et les étoiles.

    Mais un jour il tomba, épuisé et fébrile,
    Lassé de sillonner l’habitude stérile ;
    Les ailes enflammées transperçèrent la neige,
    La routine apaisée couvrit le sacrilège.

    Les fleurs de l'églantier, les sonnets parfumés
    Jaunirent dans l’oubli incertain du passé ;
    Et le temps défilait, répétitif et morne
    Dans la steppe sans fin d’un océan sans borne.

    Quelques années plus tard, qui semblèrent des siècles,
    Apportée par le souffle empourpré des pastèques,
    Il revit un matin la douce main des rimes
    Qui flottait librement au-dessus de l’abîme.

    Le délire appelant son suivant comme un frère,
    Dans l’encre délivrée les strophes s’engouffrèrent !
    Depuis il s'étourdit entre songe et réveil,
    Epuisé par sa muse aux fougues sans pareilles ;

    Telle un verre d’alcool dans un brasier pâli,
    Son coeur a ravivé le poète endormi.
    Tu me diras mais qui est son farouche époux ?
    Ce n’est ni toi ni moi, mais c’est un peu de nous.
#+END_VERSE
#+END_CENTER

* Bourgeon

#+BEGIN_CENTER
#+BEGIN_VERSE
    Si tu n’as plus le grand frisson
    En entendant le dernier son
    Si tu n’as plus le grand frisson
    Si tout n’est plus qu’une impression

    Libère le feu des glaçons
    En entendant le dernier son
    Libère le feu des glaçons
    Si tout n’est plus qu’une impression

    Ne sens-tu pas ce sol vibrer
    T’inviter à la résonance
    C’est la lave sous le plancher
    Qui cherche la bonne fréquence

    Une éruption de source chaude
    Qui vient disloquer la banquise
    Propagation de l'émeraude
    Sur la terre déjà conquise

    Laisse éclore en toi cette vie
    Elle nous vient du fond des âges
    Pour nous dire ne sois plus sage
    Laisse éclore en toi cette vie

    Elle nous vient du fond des âges
    Nous dire rien n’a d’importance
    Un temple ne vaut pas la danse
    Existe et cesse d'être sage

    Le marbre même est décadence
    Il faut ouvrir tous les tiroirs
    Et rire au nez de ce qu’on pense
    Range le couvercle au placard

    Et que s'échappe la vapeur
    Ne sens-tu pas ce sol germer
    C’est la lave sous le plancher
    Qui met la pression au moteur

    A explosion géologique
    Tourne la manivelle tourne
    Dans la ronde psychédélique
    Fais comme tu veux j’y retourne

    Il faut ouvrir tous les tiroirs
    Le marbre même est décadence
    Range le couvercle au placard
    Un temple ne vaut pas la danse

    Non vraiment rien n’a d’importance
    Que de partager la chaleur
    Laisser s'échapper la vapeur
    Et rire au nez de ce qu’on pense
#+END_VERSE
#+END_CENTER

* Les éclairs

#+BEGIN_CENTER
#+BEGIN_VERSE
    Quand le soleil s’endort sous un nuage gris

    La succession des jours n’est qu’une longue nuit
    Où la lune en plongée sous les vagues d'ébène
    Va rejoindre la voûte en son vase abyssal.

    On croirait le néant si quelque rare veine
    N'épanchait son sang clair dans l’encre immémoriale,
    Inguérissables plaies à l'éclatant débit.

    Nous laissons défiler les horloges murales,
    Entamant chaque jour le même vieux circuit,
    Attendant un éclair pour briser nos rengaines.

    Et lorsqu’avec son arc il foudroie notre ennui
    Nous restons stupéfaits, n’en croyant pas l’aubaine,
    La voix tétanisée par le trait sidéral ...

    Mais déjà il rejoint son lointain piédestal.
#+END_VERSE
#+END_CENTER

* Le premier jupon du printemps

#+BEGIN_CENTER
#+BEGIN_VERSE
    Sens-tu cet air radieux qui nous vient du soleil,
    Cet air qui carillonne au rythme des abeilles ?

    C’est le clairon des cieux, cuivré d’or et d’azur
    Qui chasse en s'étirant les froides engelures
    Jusqu’au fond des terriers creusés par le sommeil.

    C’est la flûte engourdie qui mande aux fleurs rebelles
    D'émerger des draps blancs glacés de la Nature,
    Nous dévoilant ainsi sa fine chevelure,
    Toute boucles teintées, soyeuse et sensuelle.

    L’hiver cède la place aux douces mandolines,
    À la verte fraîcheur de délicieux parfums ;
    Une pluie de cuicuis et d’essences câlines
    Recouvre les jardins, les vallées, les collines
    De teintes et de thym.

    Sens-tu cet air joyeux qui regorge de rire,
    Cet éther bourdonnant, cette ruche en délire
    Au miel folâtre et pétillant ;
    Ce facétieux coquin, léger mais plein d’audace
    Dont le souffle insistant soulève puis enlace
    Le premier jupon du printemps ?

    Regarde-les valser et se mettre à leur aise,
    On dirait un kiwi qui courtise une fraise
    La mésange s’enfuit puis freine des deux ailes
    Lui en fripon farceur, elle en fine dentelle,
    Le pinson vise au coeur le nid sous la flanelle,

    le fripon, la jupette,
    Dans leur lice se taquiner les ailes,
    Volez pinsons, volez fauvette !
    Au plus léger, vainqueur de ce duel,
    Le nid douillet tout de flanelle
    Gonflées comme une fière goélette
    Que leur jeu nous révèle au sommet des gambettes.

    Mais le plus beau de l’aquarelle
    Est sans conteste la rougeur
    Qui teinte la peau de la belle,
    De la belle aux yeux voyageurs.
    Sous le clin d’oeil du sang farceur,
    La chair s’empourpre de grenade,
    Le soir la ceint de sa cascade
    Jusqu’au flamboiement des couleurs.

    Tant est bien ficelé le piège
    Que la main n’ose même plus
    Tenter le geste sacrilège
    De reprendre le terrain nu
    A cette lumière éclatante.
    C’est sous ces rayons qui s’agrègent
    En un écho intense et dru,
    C’est dans cette trame envoûtante
    Que nait la saison frissonnante.

    Sens-tu cette flèche fidèle
    Qui s’en revient pour nous guérir
    De son céleste onguent de cire ?
    C’est l’aileron de l’hirondelle
    Qui sème doucement sa sève
    Sur le treillis de nos tonnelles
    Rongées par l’hiver qui s’achève.
    Et soudain tout n’est plus qu’un rêve,
    L’air s’est figé, comme en suspend
    Au premier baiser du printemps.
#+END_VERSE
#+END_CENTER



[[../index.php][Accueil]]
